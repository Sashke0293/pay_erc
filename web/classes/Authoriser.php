<?php
/**
 * Created by PhpStorm.
 * User: eremin_an
 * Date: 30.03.2016
 * Time: 10:34
 */

class Authoriser {
    private static $adapterConfigs = array(
        'vk' => array(
            'client_id'     => '3774741',
            'client_secret' => '3nLWEs45iWeKypmVR2CU',
            'redirect_uri'  => 'http://localhost/auth/?provider=vk'
        ),
        'odnoklassniki' => array(
            'client_id'     => '168635560',
            'client_secret' => 'C342554C028C0A76605C7C0F',
            'redirect_uri'  => 'http://localhost/auth?provider=odnoklassniki',
            'public_key'    => 'CBADCBMKABABABABA'
        ),
        'mailru' => array(
            'client_id'     => '770076',
            'client_secret' => '5b8f8906167229feccd2a7320dd6e140',
            'redirect_uri'  => 'http://localhost/auth/?provider=mailru'
        ),
        'yandex' => array(
            'client_id'     => 'bfbff04a6cb60395ca05ef38be0a86cf',
            'client_secret' => '219ba8388d6e6af7abe4b4b119cbee48',
            'redirect_uri'  => 'http://localhost/auth/?provider=yandex'
        ),
        'google' => array( // valid
            'client_id'     => '257625193226-llag4jldgdo4nuo0hgl2itgv5285feel.apps.googleusercontent.com',
            'client_secret' => '7JX4M82IfW3dxwV0jBbbZWWE',
            'redirect_uri'  => 'http://pay.erc.tt/socialResponce',
            'url'           => 'https://accounts.google.com/o/oauth2/'
        ),
        'facebook' => array(
            'client_id'     => '613418539539988',
            'client_secret' => '2deab137cc1d254d167720095ac0b386',
            'redirect_uri'  => 'http://localhost/auth?provider=facebook'
        )
    );

    private static function getInfo($url, $params){

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, urldecode(http_build_query($params)));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($curl);
        curl_close($curl);

        $tokenInfo = json_decode($result, true);

        return $tokenInfo;
    }

    public static function getUrl(){

        $params = array(
            'redirect_uri'  => self::$adapterConfigs['google']['redirect_uri'],
            'response_type' => 'code',
            'client_id'     => self::$adapterConfigs['google']['client_id'],
            'scope'         => 'https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile'
        );

        return self::$adapterConfigs['google']['url'].'auth' . '?' . urldecode(http_build_query($params));
    }

    public static function auth($code){

        $result = false;

        $params = array(
            'client_id'     => self::$adapterConfigs['google']['client_id'],
            'client_secret' => self::$adapterConfigs['google']['client_secret'],
            'redirect_uri'  => self::$adapterConfigs['google']['redirect_uri'],
            'grant_type'    => 'authorization_code',
            'code'          => $code
        );

        $tokenInfo = self::getInfo(self::$adapterConfigs['google']['url'].'token', $params);

        if (isset($tokenInfo['access_token'])) {
            $params['access_token'] = $tokenInfo['access_token'];
            $userInfo = json_decode(file_get_contents('https://www.googleapis.com/oauth2/v1/userinfo' . '?' . urldecode(http_build_query($params))), true);
            if (isset($userInfo['id'])) {
                $result = true;
            }
        }

        if($result == true){
            return $userInfo;
        }else{
            return false;
        }

    }

}