<?php
/**
 * Created by PhpStorm.
 * User: eremin_an
 * Date: 10.03.2016
 * Time: 16:14
 */

use Symfony\Component\Validator\Constraints as Assert;

class Ajax{

    private $app;
    private $converter;

    public function __construct($app){
        $this->app = $app;
        $this->converter = new Converter($app);
    }

    public function lazyService($data){
        $step = $data['step'];
        $offset = $data['offset'];
        $service_id = $data['service_id'];

        $sqlGroups = "
                    SELECT * FROM (SELECT www_name,
                           name,
                           active,
                           provider_id,
                           service_id,
                           service_type_id
                    FROM t_service
                    WHERE active = 1
                    AND service_type_id = $service_id
                    ORDER BY service_id) as ts
					JOIN t_provider tp ON tp.provider_id = ts.provider_id
                    GROUP BY ts.www_name
                    HAVING count(ts.www_name) > 1 ORDER BY ts.service_id;";

        $sqlAlone = "SELECT ts.service_type_id,
                               ts.service_id,
                               ts.active,
                               ts.provider_id,
                               ts.www_name,
                               tp.inn

                        FROM
                         t_service ts
                         INNER JOIN t_provider tp ON tp.provider_id = ts.provider_id
                        WHERE active = 1
                             AND service_type_id = $service_id
                             AND www_name not in
                              (SELECT www_name
                                FROM t_service
                                WHERE active = 1
                                  AND service_type_id = $service_id
                                   GROUP BY www_name
                                   HAVING count(*) > 1)";


//        $this->app['dbs']['pay']->executeQuery("SET SESSION sql_mode = 'NO_ENGINE_SUBSTITUTION'");
        $Groups = $this->app['dbs']['pay']->fetchAll($sqlGroups);
        $Alones = $this->app['dbs']['pay']->fetchAll($sqlAlone);

        $services = array();

        foreach($Alones as $key => $alone){
            $services[] = array(
                'service_id' => $alone['service_id'],
                'www_name' => $alone['www_name'],
                'name' => $alone['name'],
                'service_type_id' => $alone['service_type_id'],
                'inn' => $alone['inn'],
                'provider_bank_account' => $alone['provider_bank_account'],
                'groupped' => false
            );
        }
        foreach($Groups as $key => $group){
            $services[] = array(
                'service_id' => $group['service_id'],
                'www_name' => $group['www_name'],
                'name' => $group['name'],
                'service_type_id' => $group['service_type_id'],
                'groupped' => true
            );
        }
//        $sql = "SELECT ts.*, tp.inn FROM t_service ts JOIN t_provider tp ON tp.provider_id = ts.provider_id WHERE service_type_id = $service_id and ts.active = 1 ORDER BY www_name LIMIT $offset,$step;";
//        $post = $this->app['dbs']['pay']->fetchAll($sql);

        return $services;

    }

    public function getIndications($data){

        $ls = $data['ls'];
        $provider = $data['provider'];

        $sql = 'select * from ipu_input where user_ls=? AND ipu_pr_id=? ORDER by sys_date DESC LIMIT 1';
        $indications = $this->app['dbs']['wp']->fetchAll($sql, array($ls, $provider));
        return json_encode(
            array(
                'status' => 'success',
                'indications' => $indications
            )
        );
    }


    public function getServices($data){
        $service_id = $data['service_id'];

        $sqlGroups = "
                    SELECT * FROM (SELECT www_name,
                           name,
                           active,
                           provider_id,
                           service_id,
                           service_type_id
                    FROM t_service
                    WHERE active = 1
                    AND service_type_id = $service_id
                    ORDER BY service_id) as ts
					JOIN t_provider tp ON tp.provider_id = ts.provider_id
                    GROUP BY ts.www_name
                    HAVING count(ts.www_name) > 1 ORDER BY ts.service_id;";

        $sqlAlone = "SELECT ts.service_type_id,
                           ts.service_id,
                           ts.active,
                           ts.provider_id,
                           ts.provider_bank_account,
                           ts.www_name,
                           ts.name,
                           tp.inn
                    FROM t_service ts
                    JOIN t_provider tp ON tp.provider_id = ts.provider_id
                    WHERE ts.active = 1
                      AND ts.service_type_id = $service_id
                    GROUP BY ts.www_name
                    HAVING count(ts.www_name) = 1 ORDER BY service_id;";


        $this->app['dbs']['pay']->executeQuery("SET SESSION sql_mode = 'NO_ENGINE_SUBSTITUTION'");
        $Groups = $this->app['dbs']['pay']->fetchAll($sqlGroups);
        $Alones = $this->app['dbs']['pay']->fetchAll($sqlAlone);

        $services = array();

        foreach($Alones as $key => $alone){
            $services[] = array(
                'service_id' => $alone['service_id'],
                'www_name' => $alone['www_name'],
                'name' => $alone['name'],
                'service_type_id' => $alone['service_type_id'],
                'inn' => $alone['inn'],
                'provider_bank_account' => $alone['provider_bank_account'],
                'groupped' => false
            );
        }
        foreach($Groups as $key => $group){
            $services[] = array(
                'service_id' => $group['service_id'],
                'www_name' => $group['www_name'],
                'name' => $group['name'],
                'service_type_id' => $group['service_type_id'],
                'groupped' => true
            );
        }
//        $sql = "SELECT ts.*, tp.inn FROM t_service ts JOIN t_provider tp ON tp.provider_id = ts.provider_id WHERE service_type_id = $service_id and ts.active = 1 ORDER BY www_name LIMIT $offset,$step;";
//        $post = $this->app['dbs']['pay']->fetchAll($sql);

        return $services;
    }

    public function getId($string){
        $string = explode('_',$string);
        return $string[1];
    }

    private function prepareValidator($array){

    }

    public function deletePayment($data){

        $payments = $this->app['session']->get('payments');
        unset($payments[$data['id']]);
        $this->app['session']->set('payments', $payments);

        return json_encode(array('status' => 'success'));
    }

    public function checkParameter($id, $value, &$checking){

        $checked = true;

        $sql = "SELECT * FROM t_parameter WHERE parameter_id = ?;";
        $post = $this->app['dbs']['pay']->fetchAssoc($sql, array((int) $id));

        if($post['name'] == 'Л/СЧЕТ'){
            $checking['account'] = $value;
        }

        $errors = null;
        if($post['mandatory'] == '1' || $value != ''){
            if($post['mask'] != ''){
                if($post['mask'] != '205 - ТЕХНИЧЕСКОЕ ОБСЛУЖИВАНИЕ;1;'){
                    $mask = explode(';', $post['mask']);
                    $mask = $mask[0];

                    $mask = str_replace('0', '\d', $mask);
                    $mask = str_replace('9', '\d', $mask);
                    $mask = str_replace('+', '\+', $mask);
                    $mask = str_replace('(', '\(', $mask);
                    $mask = str_replace(')', '\)', $mask);

                    if(preg_match('/'.$mask.'/', $value) === 0){
                        $errors = 'Не верное значение: '.$post['min_len'];
                        $checked = false;
                    }
                }

            }else{
                if(strlen($value) < $post['min_len']){
                    $errors = 'Символов недостаточно нужно минимум: '.$post['min_len'];
                    $checked = false;
                }
                if(strlen($value) > $post['max_len']){
                    $errors = 'Символов слишком много нужно не больше: '.$post['min_len'];
                    $checked = false;
                }
            }
        }

        return array(
            'name' => $post['name'],
            'errors' => $errors,
            'checked' => $checked
        );

    }

    public function calcPayment($service_id, $value){

        $sql = "SELECT ts.*, tp.inn, tf.fee as fee1, tf.max_fee as max_fee1, tf.min_fee as min_fee1, t2f.fee as fee2, t2f.max_fee as max_fee2, t2f.min_fee as min_fee2, tf.fee_base as base FROM t_service ts JOIN t_provider tp ON tp.provider_id = ts.provider_id JOIN t_fee tf ON tf.fee_id = ts.extFee_fee_id JOIN t_fee t2f ON t2f.fee_id = ts.ext2Fee_fee_id WHERE ts.service_id = ? and ts.active = 1;";
        $serviceAll = $this->app['dbs']['pay']->fetchAll($sql, array((int) $service_id));

        $payConfig = array();
        $payConfig['new_money_pay'] = null;
        $payConfig['val'] = $value;
        $payConfig['comission'] = $serviceAll[0]['fee1'] + $serviceAll[0]['fee2'];
        $payConfig['min'] = $serviceAll[0]['min_sum'];
        $payConfig['max'] = $serviceAll[0]['max_sum'];
        $payConfig['minfee'] = $serviceAll[0]['min_fee1'] + $serviceAll[0]['min_fee2'];
        $payConfig['paymenttype'] = $serviceAll[0]['base'];

        $moneyError = null;

        if($value == 0 || $value == '' || $value == '0'){
            $moneyError = "Введите сумму!";
        }

        if($payConfig['paymenttype'] == 'PAYMENT'){
            if($payConfig['val'] * $payConfig['comission'] / 100 < $payConfig['minfee']){
                $payConfig['new_money_pay'] = $payConfig['minfee'] + $payConfig['val'];
            }else{
                $payConfig['new_money_pay'] = $payConfig['val'] * $payConfig['comission'] / 100 + $payConfig['val'];
            }
        }else if($payConfig['paymenttype'] == 'CHECK'){
            $payConfig['new_money_pay'] = ($payConfig['val']/(100-$payConfig['comission'])) * 100;
        }else{
            $moneyError = 'Неизвестная ошибка, письмо с ней отправлено!';
        }

        if($payConfig['new_money_pay'] < (int)$payConfig['min']){
            $moneyError = 'Введена недостаточная сумма!';
        }
        if($payConfig['new_money_pay'] > (int)$payConfig['max']){
            $moneyError = 'Введена слишком большая сумма!';
        }

        $payConfig['new_money_pay'] = floor($payConfig['new_money_pay'] * 100) / 100;

        return array(
            'value' => $payConfig['new_money_pay'],
            'entered' => (float)$value,
            'errors' => $moneyError
        );

    }

    public function updateTemplate($data){

        $params = array();

        foreach($data['form'] as $key => $value){

            if(preg_match('/parameter_/',$key) !== 0){

                $payId = $this->getId($key);

                $result = $this->checkParameter($payId, $value, $checking);

                $params[] .= $result['name'].': '.$value;

            }
        }

        $params = implode(';', $params);

        $this->app['dbs']['pay']->update("payment_templates", array(
            "rusr_rusr_id" => $this->app['session']->get('login_id'),
            "service_id" => $data['form']['service_id'],
            "summ" => $data['form']['money_send'],
            "params" => $params
        ), array(
            'tmp_id' => $data['id']
        ));

        return json_encode(array('status' => 'success'));

    }

    public function deleteTemplate($data){

        $id = $data['id'];

        $this->app['dbs']['pay']->delete("payment_templates", array(
            "tmp_id" => $id
        ));

        return json_encode(array('status' => 'success'));

    }












    private function toBasket($payment){
        if(isset($payment['errors'])){
            if(count($payment['errors']) > 0){
                return json_encode(array(
                    'status' => 'error',
                    'errors' => $payment['errors']
                ));
            }
        }else{
            $payments = $this->app['session']->get('payments');
            $payments[] = $payment;
            $this->app['session']->set('payments', $payments);
            return json_encode(array('status' => 'success'));
        }
    }
    private function toTemplate($template){

//        var_dump($template);

        $this->app['dbs']['pay']->insert("payment_templates", array(
            "rusr_rusr_id" => $this->app['session']->get('login_id'),
            "service_id" => $template['service_id'],
            "summ" => $template['summ'],
            "params" => $template['params']
        ));
    }
    private function toForm($form){

    }

    public function _addPaymentToBasket($data){
        return $this->addPaymentToBasket($data['id']);
    }

    public function _addFormToBasket($data){
        return $this->addFormToBasket($data['payment']);
    }

    // to basket
    // ---------------------PAYMENT TO BASKET----------------------
    private function addPaymentToBasket($id){ // платеж в корзину

        $this->converter->fillConverterBy('payment', $id);
        $this->toBasket($this->converter->returnSession());

        return json_encode(array('status' => 'success'));
    }
    // ------------------------------------------------------------

    // ---------------------TEMPLATE TO BASKET----------------------
    private function addTemplateToBasket($id){ // шаблон в корзину

        $this->converter->fillConverterBy('template', $id);
        $this->toBasket($this->converter->returnSession());
        return json_encode(array('status' => 'success'));

    }
    public function _addTemplateToBasket($data){
        return $this->addTemplateToBasket($data['id']);

    }
    // ------------------------------------------------------------

    public function addFormToBasket($form){ // форма в корзину

        return $this->toBasket($this->converter->getForm($form, 'session'));

    }

    //to template
    public function addPaymentToTemplate($id){ // платеж в шаблон

        $this->converter->fillConverterBy('payment', $id);
        $this->toTemplate($this->converter->returnTemplate());

    }

    // ---------------------FORM TO TEMPLATE----------------------
    public function addFormToTemplate($form){ // форма в шаблон
        $this->toTemplate($this->converter->getForm($form, 'template'));
        return json_encode(array('status' => 'success'));
    }
    public function _addFormToTemplate($form){ // форма в шаблон
        return $this->addFormToTemplate($form['payment']);

    }
    // ------------------------------------------------------------

    public function addBasketToTemplate($id){ // корзина в шаблон
        $this->converter->fillConverterBy('session', $id);
        $this->toTemplate($this->converter->returnTemplate());
    }

    //to form
    public function addPaymentToForm($id){ // платеж в форму

        $this->converter->fillConverterBy('payment', $id);
        $this->toForm($this->converter->returnForm());

    }
    public function addTemplateToForm($id){ // шаблон в форму

        $this->converter->fillConverterBy('template', $id);
        $this->toForm($this->converter->returnForm());
    }
    public function addBasketToForm($id){ // корзина в форму

        $this->converter->fillConverterBy('ssession', $id);
        $this->toForm($this->converter->returnForm());

    }

    public function liveSearch($data){ // корзина в форму

        $string = $data['string'];

        $string = str_replace(".", "", $string);
        $string = str_replace("/", "", $string);
        $string = str_replace("(", "", $string);
        $string = str_replace(")", "", $string);
        $string = str_replace("-", "", $string);
        $string = str_replace(" ", "%", $string);

        $sql = "SELECT ts.*,tp.inn FROM t_service ts JOIN t_provider tp ON tp.provider_id = ts.provider_id WHERE (ts.provider_bank_account LIKE '%".$string."%' OR tp.inn LIKE '%".$string."%' OR ts.www_name LIKE '%".$string."%') AND ts.active = 1 LIMIT 5;";
        $response = $this->app['dbs']['pay']->fetchAll($sql);

        return json_encode(array('result' => $response));

    }

    private function getToken($url){
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($curl);
        curl_close($curl);

        return json_decode($result, true);
    }
    private function getTokenPOST($url, $params){

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, urldecode(http_build_query($params)));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($curl);
        curl_close($curl);

        return json_decode($result, true);

    }

    public function checkSocialUser($data){
        $type = $data['type'];
        $confirm = $data['confirm'];
        $mail = $data['email'];
        $id = $data['id'];
        $uid = $data['uid'];

        $sql = "SELECT * FROM reg_users WHERE login = ?;";
        $row = $this->app['dbs']['pay']->fetchAssoc($sql, array($mail));

        if($row != false){ // FOUND

            if($row['auth_type'] == 'default'){
                return json_encode(array(
                    'status' => 'error',
                    'message' => 'Пользователь с таким mail уже зарегистрирован!<br/> Восстановите пароль, если забыли.'
                ));
            }

            $url = "https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=".$data['id'];

            $result = $this->getToken($url);

            if($result['sub'] == $uid){

                $user = User::getInstance(array(
                    'app' => $this->app,
                    'data' => array(
                        'source' => $type,
                        'login' => $mail,
                        'password' => $uid
                    )
                ));

                return json_encode(array(
                    'status' => 'success',
                    'user' => 'authorized'
                ));
            }else{
                return json_encode(array(
                    'status' => 'error',
                    'message' => 'Ошибка авторизации!'
                ));
            }
        }else{ // NOT FOUND
            if($confirm === 'true'){

                $url = "https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=".$data['id'];

                $result = $this->getToken($url);

                if($result['sub'] == $uid){
                    $this->app['dbs']['pay']->insert("reg_users", array(
                        "firstname" => $data['name'],
                        "middlename" => $data['soname'],
                        "login" => $mail,
                        "auth_type" => 'google',
                        "auth_key" => $uid
                    ));
                    $user = User::getInstance(array(
                        'app' => $this->app,
                        'data' => array(
                            'source' => $type,
                            'login' => $mail,
                            'password' => $uid
                        )
                    ));
                    return json_encode(array(
                            'status' => 'success',
                            'message' => 'Пользователь успешно создан'
                        )
                    );
                }else{
                    return json_encode(array(
                        'status' => 'error',
                        'message' => 'Ошибка авторизации!'
                    ));
                }
            }else{
                return json_encode(array(
                        'status' => 'not registered',
                        'data' => $data
                    )
                );
            }
        }
    }



    public function getErrorPayments(){
        $sql = "SELECT
                    psh.resulttext,
                    psh.pmntst_pmntst_id,
                    psh.date_begin,
                    pts.service_id,
                    pts.params,
                    pts.summ,
                    pts.rusr_rusr_id,
                    trn.email,
                    trn.trn_id,
                    pts.pmnt_id
                FROM
                    payment_status_hist psh
                        JOIN
                    payments pts ON pts.pmnt_id = psh.pmnt_pmnt_id
                        JOIN
                    payment_transaction ptr ON pts.pmnt_id = ptr.pmnt_pmnt_id
                        JOIN
                    transactions trn ON trn.trn_id = ptr.trn_trn_id
                WHERE
                    psh.pmnt_pmnt_id IN (SELECT
                            pmnt_pmnt_id
                        FROM
                            payment_status_hist
                        WHERE
                            pmntst_pmntst_id = 10)
                        AND psh.pmnt_pmnt_id NOT IN (SELECT
                            pmnt_pmnt_id
                        FROM
                            payment_status_hist
                        WHERE
                            pmntst_pmntst_id = 7);";
        $payments = $this->app['dbs']['pay']->fetchAll($sql);
        return json_encode(array(
                'payments' => $payments,
                'status' => 'success'
            )
        );
    }
    public function bigSearch($data){
        $sql = "SELECT
                    psh.resulttext,
                    psh.pmntst_pmntst_id,
                    psh.date_begin,
                    pts.service_id,
                    pts.params,
                    pts.summ,
                    pts.rusr_rusr_id,
                    trn.email,
                    trn.trn_id,
                    pts.pmnt_id
                FROM
                    payment_status_hist psh
                        JOIN
                    payments pts ON pts.pmnt_id = psh.pmnt_pmnt_id
                        JOIN
                    payment_transaction ptr ON pts.pmnt_id = ptr.pmnt_pmnt_id
                        JOIN
                    transactions trn ON trn.trn_id = ptr.trn_trn_id
                WHERE";

        if(isset($data['payment'])){
            $payment = $data['payment'];
            $sql .= " psh.pmnt_pmnt_id = $payment;";
        }else{
            $transaction = $data['transaction'];
            $sql .= " trn.trn_id = $transaction;";
        }


        $payments = $this->app['dbs']['pay']->fetchAll($sql);
        return json_encode(array(
                'payments' => $payments,
                'status' => 'success'
            )
        );
    }

    public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function checkUserExists($data){
        $mail = $data['mail'];
        if($this->checkMail($mail)){

            $md5 = md5($this->generateRandomString());

            $this->app['dbs']['pay']->insert("user_restore", array(
                "hash" => $md5,
                "rusr_rusr_id" => Converter::getUserId($mail)
            ));

            $mailer = new Mailing();
            $mailer->to = $mail;
            $mailer->subject = "Восстановление пароля";
            $mailer->type = "password";
            $mailer->data = array(
                'url' => $md5
            );

            $mailer->sendMail();

            return json_encode(array(
                    'status' => 'success',
                    'user' => 'exists'
                )
            );
        }else{
            return json_encode(array(
                    'status' => 'error',
                    'user' => 'no'
                )
            );
        }
    }

    private function checkMail($mail){
        if($mail == '' || $mail == null){
            return false;
        }
        $sql = "SELECT login FROM reg_users WHERE login = ?;";
        $user = $this->app['dbs']['pay']->fetchAssoc($sql, array($mail));

        if($user == false){
            return false;
        }else{
            return true;
        }
    }

    private function checkForm($form){

        if(isset($form['reg_login'])){
            $message = Converter::pcheckMail($form['reg_login']);

            if($message['status'] != 'success'){
                $message = $message['errors'];
                return json_encode(array(
                        'status' => 'error',
                        'id' => 'reg_login',
                        'message' => $message
                    )
                );
            }
            if($this->checkMail($form['reg_login'])){
                $message = 'Такая почта уже есть!';
                return json_encode(array(
                        'status' => 'error',
                        'id' => 'reg_login',
                        'message' => $message
                    )
                );
            }
            if($form['reg_login'] == ''){
                $message = 'Поле пустует';
                return json_encode(array(
                        'status' => 'error',
                        'id' => 'reg_login',
                        'message' => $message
                    )
                );
            }
        }
        if(isset($form['reg_pass']) && isset($form['reg_pass_2'])){
            if($form['reg_pass'] !== $form['reg_pass_2']){
                return json_encode(array(
                        'status' => 'error',
                        'id' => 'reg_pass',
                        'message' => 'Пароли не совпадают!'
                    )
                );
            }
            if(strlen($form['reg_pass']) <= 6){
                return json_encode(array(
                        'status' => 'error',
                        'id' => 'reg_pass',
                        'message' => 'Пароль должен состоять минимум из 6 символов!'
                    )
                );
            }
        }
        return true;
    }

    public function updatePassword($data){

        $form = $data['payment'];
        if($this->checkForm($form) === true){

            $hash = $form['hash'];
            $reg_pass = $form['reg_pass'];

            $sql = "SELECT * FROM user_restore WHERE hash = ? and sysdate + interval 2 day > now() order by sysdate;";
            $row = $this->app['dbs']['pay']->fetchAssoc($sql, array($hash));

            if($row != false){

                $this->app['dbs']['pay']->update("reg_users", array(
                    "password" => User::_cryptPassword($reg_pass)
                ), array(
                    'rusr_id' => $row['rusr_rusr_id']
                ));
                $sql = "DELETE FROM user_restore WHERE hash = ?;";
                $this->app['dbs']['pay']->executeQuery($sql, array($hash));

                return json_encode(array(
                        'status' => 'success',
                        'message' => 'Пароль обновлен!'
                    )
                );

            }else{

                return json_encode(array(
                        'status' => 'error',
                        'message' => 'Не могу восстановить пароль'
                    )
                );
            }

        }else{
            return $this->checkForm($form);
        }
    }

    public function sendCheck($data){
        $trn_id = $data['trn_id'];

        $mail = $this->app['session']->get('email');


        if($mail == null){
            if($this->app['session']->get('payments') != null && $this->app['session']->get('payments') != false && $this->app['session']->get('payments') != ''){
                $payments = $this->app['session']->get('payments');
                $mail = $payments[0]['Email']['value'];
                unset($payments);
            }
        }

        $exists = true;

        if($mail == null || $mail == false || $mail == ''){
            return json_encode(array(
                'status' => 'error',
                'message' => 'Не найден пользователь, обратитесь к администратору!'
            ));
        }

        ob_start();
        include "helpers/mailing/receipt.php";
        $content = ob_get_contents();
        ob_end_clean();

        if(!$exists){
            return json_encode(array(
                'status' => 'error',
                'message' => 'Транзакция не найдена!'
            ));
        }

        $mpdf=new mPDF();
        $mpdf->WriteHTML($content);
        $mpdf->Output('../pdfiles/filename.pdf','F');

        return json_encode(array(
            'status' => 'success'
        ));

    }

    public function registerForm($data){
        $form = $data['payment'];

            if($this->checkForm($form) === true){

                $url = "https://www.google.com/recaptcha/api/siteverify";

                $params = array(
                    'secret' => '6LecBRwTAAAAALYIebe_gAOSY9jEGc6IFaC6CSaX',
                    'response' => $form['g-recaptcha-response']
                );

                $verify = $this->getTokenPOST($url, $params);

                $verify = $verify['success'];

                if($verify == false){
                    return json_encode(array(
                            'status' => 'error',
                            'id' => 'signG',
                            'message' => 'Ошибка капчи'
                        )
                    );
                }else{

                    $this->app['dbs']['pay']->insert("reg_users", array(
                        "firstname" => $form['username'],
                        "middlename" => $form['usersoname'],
                        "lastname" => $form['userlastname'],
                        "login" => $form['reg_login'],
                        "password" => crypt($form['reg_pass'],'$2a$07$siteercpermrutesttesttestsalt$')
                    ));

                    $mailer = new Mailing();
                    $mailer->to = $form['reg_login'];
                    $mailer->subject = "Регистрация";
                    $mailer->type = "ticket";
                    $mailer->data = array(
                        'password' => $form['reg_pass'],
                        'login' => $form['reg_login']
                    );

                    $mailer->sendMail();

                    $user = User::getInstance(array(
                        'app' => $this->app,
                        'data' => array(
                            'source' => 'session',
                            'login' => $form['reg_login'],
                            'password' => User::pcryptPassword($form['reg_pass'])
                        )
                    ));
                    return json_encode(array(
                            'status' => 'success',
                            'message' => 'Пользователь успешно создан'
                        )
                    );
                }

            }else{
                return $this->checkForm($form);
            }

    }

    public function login($data){

        if($this->app['session']->get('email') == null && $this->app['session']->get('password') == null) {

//            var_dump($this->app['session']->get('password'));
            $user = User::getInstance(array(
                'app' => $this->app,
                'data' => array(
                    'source' => 'form',
                    'login' => $data['login'],
                    'password' => $data['password']
                )
            ));

            return json_encode(array('status' => 'success'));
        }else{
            return json_encode(
                array(
                    'status' => 'warning',
                    'message' => 'Вы уже вошли в систему'
                )
            );
        }
    }

    public function logout(){

        if(User::logout($this->app)){
            return json_encode(array('status' => 'success'));
        }else{
            return json_encode(array('status' => 'WTF'));
        }

    }
    public function search($data){

    }
}