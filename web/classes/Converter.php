<?php
/**
 * Created by PhpStorm.
 * User: eremin_an
 * Date: 24.03.2016
 * Time: 9:57
 */

class Converter {

    private $app;
    private static $_app;
    private $uid;

    private $status = false;
    private $object = array(
        'service_id' => 0,
        'params' => '',
        'email' => '',
        'summ' => 0,
        'rusr_rusr_id' => 0
    );

    public function __construct($app){
        $this->app = $app;
        self::$_app = $app;
        $this->uid = self::getUserId($this->app['session']->get('email'));
    }

    private function getRow($table, $array){
        if($table == 'payments'){
            $sql = "SELECT * FROM ".$table." WHERE ".$array['name']." = ? and rusr_rusr_id = ".$this->uid.";";
        }else{
            $sql = "SELECT * FROM ".$table." WHERE ".$array['name']." = ?;";
        }
        $row = $this->app['dbs']['pay']->fetchAssoc($sql, array((int) $array['id']));
        return $row;
    }

    private function getMail($id){
        $sql = "SELECT login FROM reg_users WHERE rusr_id = ?;";
        $mail = $this->app['dbs']['pay']->fetchAssoc($sql, array((int) $id));

        if($mail == false){
            $mail = array(
                'login' => $this->object['email']
            );
        }

        return $mail['login'];
    }
    public static function getUserId($mail){
        $sql = "SELECT rusr_id FROM reg_users WHERE login = ?;";
        $id = self::$_app['dbs']['pay']->fetchAssoc($sql, array($mail));
        return $id['rusr_id'];
    }

//    private function getUserId($mail){
//        $sql = "SELECT rusr_id FROM reg_users WHERE login = ?;";
//        $id = $this->app['dbs']['pay']->fetchAssoc($sql, array($mail));
//        return $id['rusr_id'];
//    }

    private function getService($service_id){
        $sql = "SELECT ts.*, tp.inn, tf.fee as fee1, tf.max_fee as max_fee1, tf.min_fee as min_fee1, t2f.fee as fee2, t2f.max_fee as max_fee2, t2f.min_fee as min_fee2, tf.fee_base as base FROM t_service ts JOIN t_provider tp ON tp.provider_id = ts.provider_id JOIN t_fee tf ON tf.fee_id = ts.extFee_fee_id JOIN t_fee t2f ON t2f.fee_id = ts.ext2Fee_fee_id WHERE ts.service_id = ? and ts.active = 1;";
        $service = $this->app['dbs']['pay']->fetchAssoc($sql, array((int) $service_id));
        return $service;
    }

    private function getServiceHash($service_id){

        $sql = "SELECT * FROM t_parameter WHERE service_id = ? AND param_type!='A' ORDER BY sort_order;";
        $hasharray = $this->app['dbs']['pay']->fetchAll($sql, array((int) $service_id));

        $service_hash = '';
        foreach($hasharray as $row){
            $service_hash .= $row['name'].'-'.$row['mandatory'].'-'.$row['param_type'].';';
        }

        $service_hash = mb_convert_encoding($service_hash, "CP1251", "UTF-8");

        return $service_hash;
    }

    private function getId($string){
        $string = explode('_',$string);
        return $string[1];
    }

    private function getMoneySend($summ, $service_id){
        $sql = "SELECT ts.*, tp.inn, tf.fee as fee1, tf.max_fee as max_fee1, tf.min_fee as min_fee1, t2f.fee as fee2, t2f.max_fee as max_fee2, t2f.min_fee as min_fee2, tf.fee_base as base FROM t_service ts JOIN t_provider tp ON tp.provider_id = ts.provider_id JOIN t_fee tf ON tf.fee_id = ts.extFee_fee_id JOIN t_fee t2f ON t2f.fee_id = ts.ext2Fee_fee_id WHERE service_id = ? and ts.active = 1;";
        $serviceAll = $this->app['dbs']['pay']->fetchAll($sql, array((int) $service_id));

        $payConfig = array();
        $payConfig['new_money_pay'] = null;
        $payConfig['val'] = $summ;
        $payConfig['comission'] = $serviceAll[0]['fee1'] + $serviceAll[0]['fee2'];
        $payConfig['min'] = $serviceAll[0]['min_sum'];
        $payConfig['max'] = $serviceAll[0]['max_sum'];
        $payConfig['minfee'] = $serviceAll[0]['min_fee1'] + $serviceAll[0]['min_fee2'];
        $payConfig['paymenttype'] = $serviceAll[0]['base'];

        $minFee = 0; // комиссия 1
        $min2Fee = 0; // комиссия 2

        $moneyError = null;

        if($summ == 0 || $summ == '' || $summ == '0'){
            $moneyError = "Введите сумму!";
        }

        if($payConfig['paymenttype'] == 'PAYMENT'){
            if($payConfig['val'] * $payConfig['comission'] / 100 < $payConfig['minfee']){
                $payConfig['new_money_pay'] = $payConfig['minfee'] + $payConfig['val'];
                $minFee = $serviceAll[0]['min_fee1'];
                $min2Fee = $serviceAll[0]['min_fee2'];
            }else{
                $payConfig['new_money_pay'] = $payConfig['val'] * $payConfig['comission'] / 100 + $payConfig['val'];
                $minFee = $payConfig['val'] * $serviceAll[0]['fee1'] / 100;
                $min2Fee = $payConfig['val'] * $serviceAll[0]['fee2'] / 100;
            }
        }else if($payConfig['paymenttype'] == 'CHECK'){
            if( ($payConfig['val']/(100-$payConfig['comission'])) * 100 - $payConfig['val'] < $payConfig['minfee'] ){
                $payConfig['new_money_pay'] = $payConfig['minfee'] + $payConfig['val'];
                $minFee = $serviceAll[0]['fee1'];
                $min2Fee = $serviceAll[0]['fee2'];
            }else{
                $payConfig['new_money_pay'] = ($payConfig['val']/(100-$payConfig['comission'])) * 100;
                $minFee = ($payConfig['val']/(100-$serviceAll[0]['fee1'])) * 100 - $payConfig['val'];
                $min2Fee = ($payConfig['val']/(100-$serviceAll[0]['fee2'])) * 100 - $payConfig['val'];
            }
        }else{
            $moneyError = 'Неизвестная ошибка, письмо с ней отправлено!';
        }

        if($payConfig['new_money_pay'] < (int)$payConfig['min']){
            $moneyError = 'Введена недостаточная сумма!';
        }
        if($payConfig['new_money_pay'] > (int)$payConfig['max']){
            $moneyError = 'Введена слишком большая сумма!';
        }

        $minFee = floor(ceil($minFee * 100)) / 100;
        $min2Fee = floor(ceil($min2Fee * 100)) / 100;
        $payConfig['new_money_pay'] = floor(ceil($payConfig['new_money_pay'] * 100)) / 100; // для меня это открытие, но floor косячит при округлении

        if($moneyError != null){
            return array(
                'status' => 'error',
                'errors' => $moneyError
            );
        }else{
            return array(
                'status' => 'success',
                'value' => $payConfig['new_money_pay'],
                'entered' => (float)$summ,
                'fee' => $minFee,
                'fee2' => $min2Fee
            );
        }
    }

    private function checkParam($id, $value){
        $sql = "SELECT * FROM t_parameter WHERE parameter_id = ?;";
        $parameter = $this->app['dbs']['pay']->fetchAssoc($sql, array((int) $id));
        $checked = true;
        $errors = null;
        if($parameter['mandatory'] == '1' || $value != ''){
            if($parameter['mask'] != ''){
                if($parameter['mask'] != '205 - ТЕХНИЧЕСКОЕ ОБСЛУЖИВАНИЕ;1;'){
                    $mask = explode(';', $parameter['mask']);
                    $mask = $mask[0];

                    $mask = str_replace('0', '\d', $mask);
                    $mask = str_replace('9', '\d', $mask);
                    $mask = str_replace('+', '\+', $mask);
                    $mask = str_replace('(', '\(', $mask);
                    $mask = str_replace(')', '\)', $mask);

                    if(preg_match('/'.$mask.'/', $value) === 0){
                        $errors = 'Не верное значение: '.$parameter['min_len'];
                        $checked = false;
                    }
                }
            }else{
                if(strlen($value) < $parameter['min_len']){
                    $errors = 'Символов недостаточно нужно минимум: '.$parameter['min_len'];
                    $checked = false;
                }
                if(strlen($value) > $parameter['max_len']){
                    $errors = 'Символов слишком много нужно не больше: '.$parameter['min_len'];
                    $checked = false;
                }
            }
        }

        return array(
            'errors' => $errors,
            'checked' => $checked
        );

    }

    private function squeezePayment($payment){ // params object to params string
        $params = array();
        $errors = array();
        $checked = true;

        foreach($payment as $key => $param){

            if(preg_match('/parameter_/',$key) !== 0){

                $parameter_id = $this->getId($key);

                $parameter = $this->getRow('t_parameter', array(
                    'name' => 'parameter_id',
                    'id' => $parameter_id
                ));

                $params[] = array(
                    $parameter['name'] => $param
                );

                $result = $this->checkParam($parameter_id, $param);

                if($result['errors'] != null){
                    $errors[$key] = $result['errors'];
                    $checked = false;
                }

            }
        }

        $mail = $this->checkMail($payment['Email']);
        if($mail['status'] == 'error'){
            $errors['Email'] = $mail['errors'];
            $checked = false;
        }

        if(isset($payment['money_send'])){
            if($payment['money_send'] != '' && $payment['money_send'] != null){
                $money = $this->getMoneySend($payment['money_send'], $payment['service_id']);
                if($money['status'] == 'error'){
                    $errors['money_send'] = $money['errors'];
                    $checked = false;
                }
            }else{
                $errors['money_send'] = 'Пустое поле!';
                $checked = false;
            }
        }

        if(isset($payment['agreed'])){
            if($payment['agreed'] != 'on'){
                $errors['agreed'] = 'Изъявите согласие!';
                $checked = false;
            }
        }else{
            $errors['agreed'] = 'Изъявите согласие!';
            $checked = false;
        }

        $params = $this->implodeParams($params);

        if(!$checked){
            return array('status' => 'error', 'errors'=> $errors, 'params' => $params);
        }else{
            return $params;
        }

    }

    private function simplifyParams($result){
        foreach($result as $key => $param){

            if(preg_match('/parameter_/',$key) !== 0){


                $result[$key] = $result[$key]['value'];

            }
        }
        return $result;
    }

    private function getParams($service_id){
        $sql = "SELECT * FROM t_parameter WHERE service_id = ?;";
        $parameters = $this->app['dbs']['pay']->fetchAll($sql, array((int) $service_id));

        return $parameters;
    }

    private function explodeParams($params){

        $params = explode(';',$params);

        foreach($params as $key => $value){
            $params[$key] = explode(': ', $value);
        }

        return $params;
    }

    private function implodeParams($params){

        foreach($params as $key => $value){
            $params[$key] = implode(': ', array(key($value),$value[key($value)]));
        }

        $params = implode('; ', $params);

        return $params;
    }

    private function mergeParams($base, $object){

        $resultParams = array();

        foreach($base as $bkey => $bvalue){
            foreach($object as $okey => $ovalue){
                if(trim($ovalue[0]) == trim($bvalue['name'])){
                    $resultParams['parameter_'.$bvalue['parameter_id']] = $ovalue[1];
                }
            }
        }
        return $resultParams;
    }

    private function extendParams($service_id){ // params string to params object

        $baseParams = $this->getParams($service_id);

        $objectParams = $this->explodeParams($this->object['params']);

        return $this->mergeParams($baseParams, $objectParams);

    }

    private function getTemplate($id){
        $template = $this->getRow('payment_templates', array(
            'name' => 'tmp_id',
            'id' => $id
        ));
        return $template;
    }

    private function getPayment($id){
        $template = $this->getRow('payments', array(
            'name' => 'pmnt_id',
            'id' => $id
        ));
        return $template;
    }

    private function getSession($id){
        $payments = $this->app['session']->get('payments');
        return $payments[$id];
    }

    private function paramsToForm($params){
        $temp_params = array();
        foreach ($params as $key => $value){
            $temp_params['parameter_'.$value['parameter_id']] = $value;
        }
        return $temp_params;
    }

    public function getEmptyForm($service_id){

        $params = $this->paramsToForm($this->getParams($service_id));

        $form = array(
            'params' => $params,
            'service' => $this->getService($service_id),
            'hash' => $this->getServiceHash($service_id)
        );

        return $form;
    }

    // main functions

    public function getForm($form, $destination = null){

        $this->object = array(
            'service_id' => $form['service_id'],
            'email' => $form['Email'],
            'params' => $this->squeezePayment($form),
            'summ' => $form['money_send'],
            'rusr_rusr_id' => $form['Email']
        );

        if(gettype($this->object['params']) == 'string'){
            $function = 'return'.ucfirst($destination);
            return $this->$function();
        }else{
            if(count($this->object['params']['errors']) > 0){
                return array(
                    'status' => 'error',
                    'errors' => $this->object['params']['errors']
                );
            }
        }
    }

    public static function pcheckMail($mail){
        $mailRegexp = '/^(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){255,})(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){65,}@)(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22))(?:\.(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-[a-z0-9]+)*\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-[a-z0-9]+)*)|(?:\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\]))$/iD';

        if(preg_match($mailRegexp, $mail) === 0){
            return array(
                'status' => 'error',
                'errors' => "Неверный почтовый адрес!"
            );
        }else{
            return array(
                'status' => 'success'
            );
        }
    }

    private function checkMail($mail){

        $mailRegexp = '/^(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){255,})(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){65,}@)(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22))(?:\.(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-[a-z0-9]+)*\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-[a-z0-9]+)*)|(?:\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\]))$/iD';

        if(preg_match($mailRegexp, $mail) === 0){
            return array(
                'status' => 'error',
                'errors' => "Неверный почтовый адрес!"
            );
        }else{
            return array(
                'status' => 'success'
            );
        }
    }

    public function fillConverterBy($type, $id){ // main function
        switch($type){
            case 'payment':
                $function = 'get'.ucfirst($type);
                $result = $this->$function($id);
                $this->object = array(
                    'service_id' => $result['service_id'],
                    'params' => $result['params'],
                    'summ' => $result['enter_summ'],
                    'email' => $this->getMail($result['rusr_rusr_id']),
                    'rusr_rusr_id' => $result['rusr_rusr_id']
                );
                break;
            case 'template':
                $function = 'get'.ucfirst($type);
                $result = $this->$function($id);
                $this->object = array(
                    'service_id' => $result['service_id'],
                    'params' => $result['params'],
                    'summ' => $result['summ'],
                    'email' => $this->getMail($result['rusr_rusr_id']),
                    'rusr_rusr_id' => $result['rusr_rusr_id']
                );
                break;
            case 'session':
                $result = $this->getSession($id);

                $result['Email'] = $result['Email']['value'];
                $entered = $result['money_send']['entered'];
                $result['money_send'] = $result['money_send']['entered'];

                $params = $this->squeezePayment($result);

                if(gettype($params) == 'array'){
                    $params = $params['params'];
                }

                $this->object = array(
                    'service_id' => $result['service_id'],
                    'params' => $params,
                    'summ' => $entered,
                    'email' => $result['Email'],
                    'rusr_rusr_id' => self::getUserId($result['Email'])
                );
                break;
            default:
                break;
        }
    }



    public function returnForm(){

        $params = $this->extendParams($this->object['service_id']);

        foreach($params as $key => $param){

            if(preg_match('/parameter_/',$key) !== 0){

                $parameter_id = $this->getId($key);

                $result = $this->checkParam($parameter_id, $param);

                $params[$key] = array(
                    'value' => $param
                );

                $params[$key] = array_merge($params[$key], $result, $this->getRow('t_parameter', array(
                    'name' => 'parameter_id',
                    'id' => $parameter_id
                )));

            }
        }

        $form = array(
            'params' => $params,
            'service' => $this->getService($this->object['service_id']),
            'hash' => $this->getServiceHash($this->object['service_id']),
            'email' => $this->getMail($this->object['rusr_rusr_id']),
            'summ' => $this->object['summ']
        );

        return $form;
    }

    public function returnAccount(){
        $params = $this->extendParams($this->object['service_id']);

        $baseParams = $this->getParams($this->object['service_id']);

        foreach($baseParams as $key => $value){
            if($value['sort_order'] == '1'){
                return $params['parameter_'.$value['parameter_id']];
            }
        }

        return 'no account';
    }

    public function returnTemplate(){
        return $this->object;
    }

    public function returnPayment(){
        return $this->object;
    }

    public function returnSession(){

        $service = $this->getService($this->object['service_id']);

        $moneysend = $this->getMoneySend($this->object['summ'],$service['service_id']);

        $session = array(
            'ext_fee_id' => $service['extFee_fee_id'],
            'ext2_fee_id' => $service['ext2Fee_fee_id'],
            'fee' => $moneysend['fee'],
            'fee2' => $moneysend['fee2'],
            'www_name' => $service['www_name'],
            'service_id' => $service['service_id'],
            'service_type_id' => $service['service_type_id'],
            'hash' => md5($this->getServiceHash($service['service_id'])),
            'money_send' => $moneysend,
            'agreed' => array('value' => 'on', 'errors' => null),
            'Email' => array('value' => $this->getMail($this->object['rusr_rusr_id']), 'errors' => null),
            'status' => $this->status,
        );

        $session = array_merge($session, $this->extendParams($service['service_id']));

        return $session;
    }

}