<?php


class Mailing{

    public $headers = array(
        "MIME-Version" => "1.0",
        "Content-type" => "text/html; charset=utf8",
        "From" => "ЕРЦ <erc-perm@erc-perm.ru>",
        "Subject" => "Feedback"
    );

    public $type = null; // ticket password authorised
    public $to = null;
    public $subject = null;
    public $data = null;

    public function sendMail(){

        $headers = array();
        foreach($this->headers as $key => $header){
            $headers[]= $key.": ".$header;
        }
        $headers = implode("\r\n", $headers);

        mail($this->to, $this->subject, $this->getContent($this->type), $headers);

    }

    private function getContent($type){

        ob_start();
        include "helpers/mailing/".$type.".php";
        $content = ob_get_contents();
        ob_end_clean();

        return $content;
    }

}