<?php
/**
 * Created by PhpStorm.
 * User: eremin_an
 * Date: 28.03.2016
 * Time: 12:54
 */

class User {
    public $id;
    private $email;
    public $loggedin;
    private $password;
    private $name;
    private $source;
    private $app;

    private static $_instance = null;
    private function __construct($obj) {

        $this->app = $obj['app'];
        $this->name = $obj['data']['login'];

        if($obj['data']['source'] == 'session'){
            $this->password = $obj['data']['password'];
            $this->source = 'session';
        }elseif ($obj['data']['source'] == 'form'){
            $this->password = $this->cryptPassword($obj['data']['password']);
            $this->source = 'form';
        }elseif ($obj['data']['source'] == 'google'){
            $this->password = $obj['data']['password'];
            $this->source = 'google';
        }

        if($this->login() == true){
            $this->loggedin = true;
            return array('status' => 'success');
        }else{
            $this->loggedin = false;
            return array('status' => 'error', 'message' => 'Неверный логин или пароль!');
        }

    }

    private function cryptPassword($password){
        return md5(crypt($password,'$2a$07$siteercpermrutesttesttestsalt$'));
    }
    public static function pcryptPassword($password){
        return md5(crypt($password,'$2a$07$siteercpermrutesttesttestsalt$'));
    }

    public static function _cryptPassword($password){
        return crypt($password,'$2a$07$siteercpermrutesttesttestsalt$');
    }

    public function getUser(){

        if(is_null($this->source)){
            $this->password = $this->app['session']->get('password');
            $this->source = $this->app['session']->get('auth_type');
        }

        if(in_array($this->source, array('google'))){
            $sql = "SELECT * FROM reg_users where login = '".$this->name."' and auth_key = '".$this->password."';";
        }else{
            $sql = "SELECT * FROM reg_users where login = '".$this->name."' and md5(password) = '".$this->password."';";
        }
        $user = $this->app['dbs']['pay']->fetchAssoc($sql);
        return $user;

    }

    private function saveUser($user){

        $this->app['session']->set('login_id', $user['rusr_id']);
        if(in_array($user['auth_type'], array('google'))){
            $this->app['session']->set('password', $user['auth_key']);
        }else{
            $this->app['session']->set('password', md5($user['password']));
        }
        $this->app['session']->set('auth_type', $user['auth_type']);
        $this->app['session']->set('email', $user['login']);
        $this->app['session']->set('user_name', $user['firstname']." ".$user['middlename']." ".$user['lastname']);
        $this->app['session']->set('user_status', $user['user_status']);
        $this->app['session']->set("user_email", $user['login']);
    }

    private function login(){

        if($this->source == 'session'){
            $this->password = $this->app['session']->get('password');
            $this->source = $this->app['session']->get('auth_type');
        }

        if(in_array($this->source, array('google'))){
            $sql = "SELECT * FROM reg_users where login = '".$this->name."' and auth_key = '".$this->password."';";
        }else{
            $sql = "SELECT * FROM reg_users where login = '".$this->name."' and md5(password) = '".$this->password."';";
        }
        $user = $this->app['dbs']['pay']->fetchAssoc($sql);

        if($user){
            $this->saveUser($user);
            $this->id = $user['rusr_id'];
            $this->loggedin = true;
            return true;
        }else{
            $this->loggedin = false;
            return false;
        }
    }

    public static function logout($app){
        $app['session']->remove('auth_type');
        $app['session']->remove('login_id');
        $app['session']->remove('password');
        $app['session']->remove('email');
        $app['session']->remove('user_name');
        $app['session']->remove('user_status');
        $app['session']->remove("user_email");
        return true;
    }

    public static function checkUserObject($user){
        if(gettype($user) == 'object'){
           if($user->loggedin == true){
               return true;
           }else{
               return false;
           }
        }else{
            return false;
        }
    }

    protected function __clone() {

    }
    static public function getInstance($obj) {
        if(is_null(self::$_instance))
        {
            self::$_instance = new self($obj);
        }
        return self::$_instance;
    }

}