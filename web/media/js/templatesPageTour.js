/**
 * Created by eremin_an on 10.03.2016.
 */

$(document).ready(function () {

    var tour_step = Lockr.get('tour_step');

    if(Lockr.get('tour_step') == 'undefined' || Lockr.get('tour_step') == null || Lockr.get('tour_step') == ''){
        Lockr.set('tour_step', 0);
    }

    function resetTour(){
        Lockr.set('tour_step', 0);
    }
    function incTour(){
        Lockr.set('tour_step', Lockr.get('tour_step') + 1);
    }
    function decTour(){
        Lockr.set('tour_step', Lockr.get('tour_step') - 1);
    }

    function initTour(){
        //$('#helper_disable').show();
        //Lockr.set('tour_enabled', 1);
    }

    function startTour(){

        $("tr .cart-back").first().tourTip({
            title: "Добавление платежа",
            description: "Вы можете добавить платеж в пачку, если платеж не пройдет проверку, система оповестит вас об этом",
            previous: true,
            position: 'left'
        });

        $("tr .edit-back").first().tourTip({
            title: "Редактирование шаблона",
            description: "Вы можете отредактировать шаблон или на его основе создать новый",
            previous: true,
            position: 'left'
        });

        $("tr .del-back").first().tourTip({
            title: "Удаление шаблона",
            description: "Кнопка удаляет шаблон",
            previous: true,
            position: 'left'
        });

        $(".serviceToolName").first().tourTip({
            title: "Сервис",
            description: "Сервис который вы оплачиваете",
            previous: true,
            position: 'bottom'
        });
        $(".serviceToolParams").first().tourTip({
            title: "Параметры",
            description: "Параметры который вы ввели",
            previous: true,
            close: true,
            position: 'bottom'
        });
    }

    if(Lockr.get('tour_enabled') == 1 && Lockr.get('tour_templates') == 0){
        initTour();
        startTour();
        Lockr.set('tour_templates', 1);
        $.tourTip.start({
            nextButtonText: 'Далее',
            previousButtonText: 'Назад',
            closeButtonText: 'Закрыть'
        });
    }

});