/**
 * Created by eremin_an on 23.03.2016.
 */
/**
 * Created by eremin_an on 10.03.2016.
 */

$(document).ready(function () {

    var tour_step = Lockr.get('tour_step');

    if(Lockr.get('tour_step') == 'undefined' || Lockr.get('tour_step') == null || Lockr.get('tour_step') == ''){
        Lockr.set('tour_step', 0);
    }

    function resetTour(){
        Lockr.set('tour_step', 0);
    }
    function incTour(){
        Lockr.set('tour_step', Lockr.get('tour_step') + 1);
    }
    function decTour(){
        Lockr.set('tour_step', Lockr.get('tour_step') - 1);
    }

    function initTour(){
        //$('#helper_disable').show();
        //Lockr.set('tour_enabled', 1);
    }

    function startTour(){

        $("tr .cart-back").first().tourTip({
            title: "Добавление платежа",
            description: "Вы можете снова добавить платеж в пачку",
            previous: true,
            position: 'left'
        });

        $(".tourStatus").first().tourTip({
            title: "Статус транзакции",
            description: "Статус транзакции показывает на каком этапе оплаты находится транзакция",
            previous: true,
            close: true,
            position: 'bottom'
        });
    }

    if(Lockr.get('tour_enabled') == 1 && Lockr.get('tour_history') == 0){
        initTour();
        startTour();
        Lockr.set('tour_history', 1);
        $.tourTip.start({
            nextButtonText: 'Далее',
            previousButtonText: 'Назад',
            closeButtonText: 'Закрыть'
        });
    }

});