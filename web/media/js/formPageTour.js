/**
 * Created by eremin_an on 23.03.2016.
 */
/**
 * Created by eremin_an on 10.03.2016.
 */

$(document).ready(function () {

    var tour_step = Lockr.get('tour_step');

    if(Lockr.get('tour_step') == 'undefined' || Lockr.get('tour_step') == null || Lockr.get('tour_step') == ''){
        Lockr.set('tour_step', 0);
    }

    function resetTour(){
        Lockr.set('tour_step', 0);
    }
    function incTour(){
        Lockr.set('tour_step', Lockr.get('tour_step') + 1);
    }
    function decTour(){
        Lockr.set('tour_step', Lockr.get('tour_step') - 1);
    }

    function initTour(){
        //$('#helper_disable').show();
        //Lockr.set('tour_enabled', 1);
    }

    function startTour(){

        $(".requisites").first().tourTip({
            title: "Реквизиты",
            description: "Обязательно проверяйте реквизиты банка!",
            previous: true,
            position: 'top'
        });

        $("#addPayment").tourTip({
            title: "Параметры платежа",
            description: "Введите параметры платежа, указанные в платежке",
            previous: true,
            position: 'top'
        });

        $(".toRead").first().tourTip({
            title: "Правила",
            description: "Не забудьте ознакомиться с правилами",
            previous: true,
            position: 'top'
        });

        $(".addTemplate").first().tourTip({
            title: "Шаблон",
            description: "Вы можете заполнить параметры и добавить шаблон...",
            previous: true,
            position: 'top'
        });

        $(".addPay").first().tourTip({
            title: "Добавление платежа",
            description: "Или же добавить платеж в пачку",
            previous: true,
            position: 'top'
        });

        $(".mandatory").first().tourTip({
            title: "Обязательные поля",
            description: "Поля обязательные для заполнения выделены вот такой звездой!",
            previous: true,
            position: 'right'
        });

        $(".pathback").first().tourTip({
            title: "Навигация",
            description: "При помоци навигации вы можете вернуться на один из шагов оплаты",
            previous: true,
            close: true,
            position: 'bottom'
        });

    }

    if(Lockr.get('tour_enabled') == 1 && Lockr.get('tour_form') == 0){
        initTour();
        startTour();
        Lockr.set('tour_form', 1);
        $.tourTip.start({
            nextButtonText: 'Далее',
            previousButtonText: 'Назад',
            closeButtonText: 'Закрыть'
        });
    }

});