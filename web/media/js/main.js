/**
 * Created by eremin_an on 10.03.2016.
 */

$(document).ready(function () {

    if(Lockr.get('tour_enabled') == 1){
        //$('#helper_disable').show();
    }

    function dropTour(){
        Lockr.set('tour_enabled', 0);
    }

    $('#helper_disable').click(function () {
        dropTour();
        window.location.reload();
    });

    $(window).scroll(function () {
        if ($(this).scrollTop() > 0) {
            $('#scroller').fadeIn();
        } else {
            $('#scroller').fadeOut();
        }
    });

    $('#scroller').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 400);
        return false;
    });


    function searchServices(){
        if($('input.Search').length){
            var SearchText = $('input.Search').val(); // строка поиска

            SearchText = SearchText.toLowerCase();

            var searchcount = 0;

            $('.service_container').each(function(index, val){ // проходим все сервисы вроде быстро работает
                var inn = $(val).attr('data-service-inn');
                var name = $(val).attr('data-service-name').toLowerCase();
                var pba = $(val).attr('data-service-pba');
                if(typeof inn == 'undefined'){
                    inn = '3e47b75000b0924b6c9ba5759a7cf15d';
                }
                if(typeof pba == 'undefined'){
                    pba = '3e47b75000b0924b6c9ba5759a7cf15d';
                }
                if(inn.indexOf(SearchText) > -1 || name.indexOf(SearchText) > -1 || pba.indexOf(SearchText) > -1){
                    $(val).removeClass('hidden');
                    searchcount++;
                }else{
                    $(val).addClass('hidden');
                }
            }).promise().done(function () {
                $('.search_count').text(searchcount);
            });

            $('.service_group').each(function(index, val){ // проходим все группы и скрываем те, что не содержат сервисов
                var service_containers = $(val).find('.service_container');
                var hide = true;
                $(service_containers).each(function(index, val){ // проходим все сервисы вроде быстро работает
                    //console.log($(val), $(val).hasClass('hidden'));
                    if(!$(val).hasClass('hidden')){
                        hide = false;
                    }
                });
                if(hide){
                    $(val).addClass('hidden');
                }
            });

        }
    }

    $('[data-show]').on('click',function (e) {
        e.preventDefault();
        var toggle = '[data-toggle="'+$(this).attr('data-show')+'"]';
        $(toggle).show();
    });
    $('[data-hide]').on('click',function (e) {
        e.preventDefault();
        var toggle = '[data-toggle="'+$(this).attr('data-hide')+'"]';
        $(toggle).hide();
    });

    function goToRoot(){
        window.location.reload();
    }

    $('.Search').on('input', function () {
        searchServices();
    });

    searchServices();

    function equalize(){

        $('[equalizer]').each(function () { // делает колонки одинаковой высоты
            var max_height = 0;
            $(this).find('[equal]').each(function () {
                if($(this).height() > max_height){
                    max_height = $(this).height();
                }
            });
            $(this).find('[equal]').each(function () {
                $(this).height(max_height);
            });
        });

    }

    $('[data-mask]').each(function (index, val) {
        var mask = $(val).attr('data-mask');

        if(mask == '205 - ТЕХНИЧЕСКОЕ ОБСЛУЖИВАНИЕ;1;'){
            $(val).val('205 - ТЕХНИЧЕСКОЕ ОБСЛУЖИВАНИЕ');
            $(val).attr('disabled','disabled');
        }else{
            mask = mask.split(';');
            mask = mask[0];
            mask = mask.replace(/0/gi, '9');
            $(val).inputmask(mask);
        }
    });

    equalize();

    $('.addTemplate').on('click', function (e) {
        var addPaymentForm = {};
        $.each($('#addPayment').serializeArray(), function(_, kv) {
            if (addPaymentForm.hasOwnProperty(kv.name)) {
                addPaymentForm[kv.name] = $.makeArray(addPaymentForm[kv.name]);
                addPaymentForm[kv.name].push(kv.value);
            }
            else {
                addPaymentForm[kv.name] = kv.value;
            }
        });

        $.post('/ajax',{'action': '_addFormToTemplate', data: {payment: addPaymentForm}}, function (data) {
            data = JSON.parse(data);
            if(data.status == 'success'){
                alertify.success('<a href="/templates" class="pretyLink">Шаблон</a> успешно сохранен!');
            }

        });
    });

    $('.updateTemplate').on('click', function (e) {
        var addPaymentForm = {};
        var id = $(this).attr('data-tmp-id');
        $.each($('#addPayment').serializeArray(), function(_, kv) {
            if (addPaymentForm.hasOwnProperty(kv.name)) {
                addPaymentForm[kv.name] = $.makeArray(addPaymentForm[kv.name]);
                addPaymentForm[kv.name].push(kv.value);
            }
            else {
                addPaymentForm[kv.name] = kv.value;
            }
        });

        $.post('/ajax',{'action': 'updateTemplate', data: {form: addPaymentForm, id: id}}, function (data) {
            data = JSON.parse(data);
            if(data.status == 'success'){
                alertify.success('<a href="/templates" class="pretyLink">Шаблон</a> успешно обновлен!');
            }

        });
    });


    $('.deleteTemplate').on('click', function (e) {
        var id = $(this).attr('data-tmp-id');

        var that = this;

        $.post('/ajax',{'action': 'deleteTemplate', data: {id: id}}, function (data) {
            data = JSON.parse(data);
            if(data.status == 'success'){
                alertify.success('Шаблон успешно удален!');
                $(that).parent().remove();
            }

        });
    });

    $('.addToCart').on('click', function (e) {
        var id = $(this).attr('data-tmp-id');

        var that = this;

        $.post('/ajax',{'action': '_addTemplateToBasket', data: {id: id}}, function (data) {
            data = JSON.parse(data);
            if(data.status == 'success'){
                alertify.success("Добавлено");
                setTimeout(goToRoot, 100);
            }else{
                alertify.error("Одно из полей не соответствует требованиям, отредактируйте форму!");
            }
        });
    });

    $('.addPaymentToBasket').click(function (e) {
        var id = $(this).attr('data-pmnt-id');
        $.post('/ajax', {'action': '_addPaymentToBasket', 'data': {id: id}}, function (data) {
            data = JSON.parse(data);
            if(data.status == 'success'){
                setTimeout(goToRoot, 100);
            }else{
                alertify.error("Возникли проблемы с добавлением в корзину!");
            }
        });
    });

    //function deletePayment(id){
    //    $.post('/ajax', {'action': 'deletePayment', 'data': {id: id}}, function (data) {
    //        data = JSON.parse(data);
    //        //window.location.reload()
    //    });
    //}

    $('[data-deletePayment]').on('click', function(e){
            $.post('/ajax', {'action': 'deletePayment', 'data': {id: $(this).attr('data-deletePayment')}}, function (data) {
                data = JSON.parse(data);
                //console.log(data);
                window.location.reload();
            });
    });

    $('#reg_form').submit(function (e) {
        e.preventDefault();
        var addPaymentForm = {};

        //$('#addPayment').attr('disabled', 'disabled');

        $.each($('#reg_form').serializeArray(), function(_, kv) {
            if (addPaymentForm.hasOwnProperty(kv.name)) {
                addPaymentForm[kv.name] = $.makeArray(addPaymentForm[kv.name]);
                addPaymentForm[kv.name].push(kv.value);
            }
            else {
                addPaymentForm[kv.name] = kv.value;
            }
        });

        $.post('/ajax', {'action': 'registerForm', 'data': {payment: addPaymentForm}}, function (data) {
            data = JSON.parse(data);
            if(data.status == 'success'){
                alertify.success('Регистрация прошла успешно!');
                window.location.assert('/');
            }else{
                $('#'+data.id).parent().find('.error-message').text(data.message);
                $('#'+data.id).parent().addClass('warning-box');
                $('#'+data.id).focus();
                alertify.error('Ошибка регистрации!');
            }
        });

    });

    $('#pass_form').submit(function (e) {
        e.preventDefault();
        var addPaymentForm = {};

        //$('#addPayment').attr('disabled', 'disabled');

        $.each($('#pass_form').serializeArray(), function(_, kv) {
            if (addPaymentForm.hasOwnProperty(kv.name)) {
                addPaymentForm[kv.name] = $.makeArray(addPaymentForm[kv.name]);
                addPaymentForm[kv.name].push(kv.value);
            }
            else {
                addPaymentForm[kv.name] = kv.value;
            }
        });

        $.post('/ajax', {'action': 'updatePassword', 'data': {payment: addPaymentForm}}, function (data) {
            data = JSON.parse(data);
            if(data.status == 'success'){
                alertify.success('Пароль обновлен!');
                window.location = '/';
            }else{
                $('#'+data.id).parent().find('.error-message').text(data.message);
                $('#'+data.id).parent().addClass('warning-box');
                $('#'+data.id).focus();
                alertify.error('Ошибка регистрации!');
            }
        });

    });

    function checkUserExists(mail, callback){
        $.post('/ajax', {'action': 'checkUserExists', 'data': {mail: mail}}, function (data) {
            data = JSON.parse(data);
            if(data.status == 'success'){
                alertify.success('На указанную почту выслан пароль!');
            }else{
                callback();
            }
        });
    }

    function sendCheck(trn_id){
        $.post('/ajax', {action:'sendCheck', data:{trn_id: trn_id}}, function (data) {
            data = JSON.parse(data);
            alertify.log(data.message);
        });
    }

    $('#forgotpassbtn').on('click', function (e) {
        var mail = $('#forgotpass').val();
        checkUserExists(mail, function () {
            alertify.error('Пользователя с таким email нет!');
        });
    });

    $('[data-hider]').on('click', function (e) {
        var $that = $(this);
        var $hider = $($(this).attr('data-hider'));
        var rolls = Lockr.get('rollUp');
        if(!rolls){
            rolls = {};
        }
        if($that.hasClass('flaticon-arrows-1')){
            $that.removeClass('flaticon-arrows-1');
            $that.addClass('flaticon-arrows-2');
            $hider.slideUp(500);
            rolls['s_'+$that.attr('data-hider')] = 1;
        }else{
            $that.removeClass('flaticon-arrows-2');
            $that.addClass('flaticon-arrows-1');
            $hider.slideDown(500);
            rolls['s_'+$that.attr('data-hider')] = 0;
        }
        Lockr.set('rollUp',rolls);
    });



    $('#getErrorPayments').click(function (e) {
        e.preventDefault();



        $.post('/ajax', {'action': 'getErrorPayments'}, function (data) {
            data = JSON.parse(data);
            //console.log(data);
            $('#liveresults').html('<thead><tr>' +
            '<th>Результат</th>' +
            '<th>Статус</th>' +
            '<th>Дата создания</th>' +
            '<th>Сервис ID</th>' +
            '<th>Параметры</th>' +
            '<th>Сумма</th>' +
            '<th>ID пользователя</th>' +
            '<th>Mail</th>' +
            '<th>ID транзакции</th>' +
            '<th>ID платежа</th>' +
            '</tr></thead><tbody></tbody>');
            if(data.status == 'success'){
                for(var id in data.payments){
                    //console.log(data.payments[id]);
                    var row = '<tr>';
                    for(var item in data.payments[id]){
                        console.log(item);
                        if(item == 'resulttext'){
                            row += '<td><span class="wrapCell w200">'+data.payments[id][item]+'</span></td>';
                        }else if (item == 'params'){
                            row += '<td><span class="wrapCell w150">'+data.payments[id][item]+'</span></td>';
                        }else if (item == 'email'){
                            row += '<td><span class="wrapCell w150">'+data.payments[id][item]+'</span></td>';
                        }else{
                            row += '<td><span class="wrapCell">'+data.payments[id][item]+'</span></td>';
                        }
                    }
                    row += '</tr>';
                    $('#liveresults > tbody').append(row);
                }

                $('#liveresults').first().DataTable();

            }else{
                console.log(123);
            }
        });
    });
    $('#bigSearch').click(function (e) {
        e.preventDefault();

        var data = {};

        $transaction = $('#transactionID');
        if(!$transaction.val()){
            $payment = $('#paymentID');
            if($payment.val()){
                data.payment = $payment.val();
            }else{
                return;
            }
        }else{
            data.transaction = $transaction.val();
        }

        $.post('/ajax', {'action': 'bigSearch', data:data}, function (data) {
            data = JSON.parse(data);
            //console.log(data);
            $('#liveresults').html('<thead><tr>' +
            '<th>Результат</th>' +
            '<th>Статус</th>' +
            '<th>Дата начала</th>' +
            '<th>Сервис ID</th>' +
            '<th>Параметры</th>' +
            '<th>Сумма</th>' +
            '<th>ID пользователя</th>' +
            '<th>Mail</th>' +
            '<th>ID транзакции</th>' +
            '<th>ID платежа</th>' +
            '</tr></thead><tbody></tbody>');
            if(data.status == 'success'){
                for(var id in data.payments){
                    //console.log(data.payments[id]);
                    var row = '<tr>';
                    for(var item in data.payments[id]){
                        console.log(item);
                        if(item == 'resulttext'){
                            row += '<td><span class="wrapCell w200">'+data.payments[id][item]+'</span></td>';
                        }else if (item == 'params'){
                            row += '<td><span class="wrapCell w150">'+data.payments[id][item]+'</span></td>';
                        }else if (item == 'email'){
                            row += '<td><span class="wrapCell w150">'+data.payments[id][item]+'</span></td>';
                        }else{
                            row += '<td><span class="wrapCell">'+data.payments[id][item]+'</span></td>';
                        }
                    }
                    row += '</tr>';
                    $('#liveresults > tbody').append(row);
                }

                $('#liveresults').first().DataTable();
            }else{
                console.log('error no data');
            }
        });
    });

    function parseIndications(data){
        var indicationsObj = {};
        console.log(data);
        var serviceArray = [
            {name: 'GAZ', descr: 'GAZ'},
            {name: 'EL', descr: 'EL'},
            {name: 'GB', descr: 'GB'},
            {name: 'XB', descr: 'XB'}
        ];
        for(var i=1; i<5; i++){
            for(var j in serviceArray){
                try{
                    if(typeof indicationsObj[serviceArray[j].descr] == 'undefined'){
                        indicationsObj[serviceArray[j].descr] = [];
                    }
                    indicationsObj[serviceArray[j].descr].push({
                        'from': data[serviceArray[j].name+i+'_OT'] || '',
                        'to': data[serviceArray[j].name+i+'_DO'] || ''
                    });
                }catch(e){

                }
            }
        }
        return indicationsObj;
    }

    $('.getIndications').click(function (e) {
        e.preventDefault();

        var data = {
            provider: $('#provider').val(),
            ls: $('#personal_account').val()
        };

        $.post('/ajax', {'action': 'getIndications', data:data}, function (data) {
            data = JSON.parse(data);
            var indications = parseIndications(data.indications[0]);

            console.log(indications);

            var tableHeader = '<tr>' +
                '<th></th>' +
                '<th>От</th>' +
                '<th>До</th>' +
                '<th>От</th>' +
                '<th>До</th>' +
                '<th>От</th>' +
                '<th>До</th>' +
                '<th>От</th>' +
                '<th>До</th>' +
                '</tr>';
            tableHeader += '<tr>' +
                '<th></th>' +
                '<th colspan="2">1</th>' +
                '<th colspan="2">2</th>' +
                '<th colspan="2">3</th>' +
                '<th colspan="2">4</th>' +
                '</tr>';

            var serviceArray = {
                'GAZ': 'Газ',
                'EL': 'Электричество',
                'GB': 'Горячая вода',
                'XB': 'Холодная вода'
            };

            $('.indicationsTable').html('');
            $('.indicationsTable').append(tableHeader);
            for(var ind in indications){

                var count = 0;

                var tabelRow = '<tr>' +
                    '<td>' +
                    serviceArray[ind] +
                    '</td>';

                for(var type in indications[ind]){
                    count++;
                    tabelRow += '<td>' +
                    '<div class="input-group">' +
                    '<input type="text" id="'+ind+count+'_OT'+'" name="'+ind+count+'_OT'+'" value="'+indications[ind][type].from+'" class="input"/>' +
                    '</div>' +
                    '<td>' +
                    '<div class="input-group">' +
                    '<input type="text" id="'+ind+count+'_DO'+'" name="'+ind+count+'_DO'+'" value="'+indications[ind][type].to+'" class="input"/>' +
                    '</td>';
                }
                tabelRow += '</tr>';
                $('.indicationsTable').append(tabelRow);
            }
        });
    });

    $('#cleartable').click(function (e) {
        $('#liveresults').html('');
    });


    $('#addPayment').submit(function (e) {
        e.preventDefault();
        var addPaymentForm = {};

        //$('#addPayment').attr('disabled', 'disabled');

        $.each($('#addPayment').serializeArray(), function(_, kv) {
            if (addPaymentForm.hasOwnProperty(kv.name)) {
                addPaymentForm[kv.name] = $.makeArray(addPaymentForm[kv.name]);
                addPaymentForm[kv.name].push(kv.value);
            }
            else {
                addPaymentForm[kv.name] = kv.value;
            }
        });

        if(typeof addPaymentForm['agreed'] == 'undefined'){
            addPaymentForm['agreed'] = 'off';
        }

        $.post('/ajax', {'action': '_addFormToBasket', 'data': {payment: addPaymentForm}}, function (data) {
            data = JSON.parse(data);
            //console.log(data);
            if(data.status == 'success'){
                alertify.success('Платеж добавлен');
                setTimeout(goToRoot, 100);
            }else{
                for(parameter in addPaymentForm){
                    if(typeof data['errors'][parameter] != 'undefined'){
                        $('#'+parameter).css('border','1px solid red');
                        $('#'+parameter).parent().find('.error-message').text(data['errors'][parameter]);
                        $('#'+parameter).parent().addClass('warning-box');
                        $('#'+parameter).focus();
                        break;
                    }else{
                        $('#'+parameter).css('border','1px solid green');
                        $('#'+parameter).parent().find('.error-message').text('');
                        $('#'+parameter).parent().removeClass('warning-box');
                    }
                }
            }
        });
    });

    $('.logout').on('click',function (e) {
        e.preventDefault();
        $.post('/ajax', {action:'logout'}, function (data) {
            data = JSON.parse(data);
            if(data.status == 'success'){
                window.location.reload();
            }
        });
    });
    $('.login').on('click',function (e) {
        e.preventDefault();
        $.post('/ajax', {action:'login',data:{'login': $('#login').val(),'password': $('#password').val()}}, function (data) {
            data = JSON.parse(data);
            if(data.status == 'success'){
                window.location.reload();
            }
        });
    });

    function equalize(){

        $('[equalizer]').each(function () { // делает колонки одинаковой высоты
            var max_height = 0;
            $(this).find('[equal]').each(function () {
                if($(this).height() > max_height){
                    max_height = $(this).height();
                }
            });
            $(this).find('[equal]').each(function () {
                $(this).height(max_height);
            });
        });
    }

    equalize();

    function startTour(){
        $(".payments").tourTip({
            title: "Оплата",
            description: "Поздравляю! В вашей пачке появился платеж...",
            previous: true,
            position: 'right'
        });
        $(".payonlinebtn").tourTip({
            title: "Кнопка оплаты",
            description: "Теперь вы можете оплатить его с помощью данной кнопки",
            previous: true,
            close: true,
            position: 'right'
        });
        $("#tourStart").tourTip({
            title: "Услуги",
            description: "Или добавить ещё платежей",
            previous: true,
            position: 'right',
            close: true
        });
    }

    $('.liveSearch').on('focus', function (e) {
        $('.liveResults').show();
    });
    $('.liveSearch').on('blur', function (e) {
        $('.liveResults').delay(100).promise().done(function (e) {
            $(this).slideUp(300);
        });
    });

    var liveResults = function (results) {
        $('.liveResults ul li').remove();
        for(var row in results){
            $('.liveResults ul').append("<li><a href='/services/"+results[row].service_type_id+"/form/"+results[row].service_id+"' target='_blank'><p class='m-5 p-0'><b>"+results[row].www_name+"</b></p> <p class='m-0 p-0'><b>ИНН: </b>"+results[row].inn+"</p> <p class='m-0 p-0'><b>Р/С: </b>"+results[row].provider_bank_account+"</p></a></li>");
        }

    }

    $('.liveSearch').on('input', function (e) {
        $.post('/ajax',{action:'liveSearch',data:{string:$('.liveSearch').val()}}, function (data) {
            data = JSON.parse(data);

            liveResults(data['result']);
        });
    });


    if(Lockr.get('tour_enabled') == 1 && Lockr.get('tour_pay') == 0 && $('.payonlinebtn').length){
        startTour();
        Lockr.set('tour_pay', 1);
        $.tourTip.start({
            nextButtonText: 'Далее',
            previousButtonText: 'Назад',
            closeButtonText: 'Закрыть'
        });
    }
    if(Lockr.get('rollUp')){
        var rolls = Lockr.get('rollUp');
        for(var roll in rolls){
            var nroll = roll.substr(2,roll.length);
            if(rolls[roll] == 1){
                var $hider = $("[data-hider='"+nroll+"']");
                $hider.removeClass('flaticon-arrows-1');
                $hider.addClass('flaticon-arrows-2');
                $(nroll).slideUp(500);
            }
        }
    }


});

// google авторизация
var googleCounter = 0;
function Google_signIn(googleUser) {
    var profile = googleUser.getBasicProfile();
    var id_token = googleUser.getAuthResponse().id_token;

    if(googleCounter < 1){
        googleCounter++;
        return;
    }

    if(typeof profile != 'undefined'){
        $.ajax({
            type: "POST",
            dataType: 'json',
            data: {action:'checkSocialUser', data:{'email': profile.getEmail(), 'id': id_token, 'uid': profile.getId(), 'name': profile.getGivenName(), 'soname': profile.getFamilyName(), 'type': 'google', 'confirm': false}},
            url: '/ajax',
            success: function(data) {
                if(data.status == 'not registered'){
                    alertify.confirm("Пользователь с почтовым ящиком "+profile.getEmail()+", ещё не создан, хотите создать?",
                        function(){
                            alertify.success('Создаю пользователя...');
                            $.post('/ajax', {
                                'action':'checkSocialUser',
                                'data':{'email': profile.getEmail(),
                                    'id': id_token,
                                    'uid': profile.getId(),
                                    'name': profile.getGivenName(),
                                    'soname': profile.getFamilyName(),
                                    'type': 'google',
                                    'confirm': true
                                }
                            }, function(data){
                                data = JSON.parse(data);
                                if(data.status == 'success'){
                                    alertify.success(data.message);
                                    window.location.reload();
                                }
                            });
                        },
                        function(){

                        });
                }else if(data.status == 'error'){
                    alertify.error(data.message);
                }else if(data.status == 'success'){
                    window.location.reload();
                }
            }
        });
    }
}