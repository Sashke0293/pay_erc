/**
 * Created by eremin_an on 15.03.2016.
 */

$(document).ready(function () {

    var lazyLoader = (function (){

        var id = 0;
        var services = [];
        var grouped_services;

        function initToolTips(){
            var tour_step = Lockr.get('tour_step');

            if(Lockr.get('tour_step') == 'undefined' || Lockr.get('tour_step') == null || Lockr.get('tour_step') == ''){
                Lockr.set('tour_step', 0);
            }

            function resetTour(){
                Lockr.set('tour_step', 0);
            }
            function incTour(){
                Lockr.set('tour_step', Lockr.get('tour_step') + 1);
            }
            function decTour(){
                Lockr.set('tour_step', Lockr.get('tour_step') - 1);
            }

            function initTour(){
                //$('#helper_disable').show();
                //Lockr.set('tour_enabled', 1);
            }

            function startTour(){

                $(".service_group").first().tourTip({
                    title: "Группа сервисов",
                    description: "Здесь представлена группа сервисов с одинаковым наименованием, будьте внимательны и <b style='color: red;'>обращайте внимание на расчетный счет</b>!",
                    previous: true,
                    position: 'top'
                });

                $(".Search.input").first().tourTip({
                    title: "Строка поиска",
                    externalContentHtml: 'Начните вводить <b>ИНН</b>, <b>название сервиса</b> или <b>расчетный счет</b> чтобы быстрее найти платеж!',
                    previous: true,
                    position: 'bottom'
                });

                $(".service_container").first().tourTip({
                    title: "Сервис",
                    description: "Давайте нажмем на сервис, чтобы оформить платеж",
                    previous: true,
                    close: true,
                    position: 'top'
                });
            }

            if(Lockr.get('tour_enabled') == 1 && Lockr.get('tour_service') == 0){
                initTour();
                startTour();
                Lockr.set('tour_service', 1);
                $.tourTip.start({
                    nextButtonText: 'Далее',
                    previousButtonText: 'Назад',
                    closeButtonText: 'Закрыть'
                });
            }
        }

        function array_group(array, field){

            var stats = [];

            for(var st in array){

                if(in_array(array[st][field], stats, field) !== false){
                    var id = in_array(array[st][field], stats, field);
                    stats[id].group.push(array[st]);
                }else{
                    var obj = {};
                    obj[field] = array[st][field];
                    obj.group_type = field;
                    obj.group = [array[st]];
                    stats.push(obj);
                }
            }

            return stats;

        }
        function in_array(needle, haystack, inner){
            inner = null || inner;
            for(var i in haystack){
                if(inner){
                    if(haystack[i][inner] == needle){
                        return i;
                    }
                }else{
                    if(haystack[i] == needle){
                        return i;
                    }
                }
            }
            return false;
        }

        function searchServices(){
            if($('input.Search').length){
                var SearchText = $('input.Search').val(); // строка поиска

                SearchText = SearchText.toLowerCase();

                var searchcount = 0;

                $('.service_container').each(function(index, val){ // проходим все сервисы вроде быстро работает
                    var inn = $(val).attr('data-service-inn');
                    var name = $(val).attr('data-service-name').toLowerCase();
                    var pba = $(val).attr('data-service-pba');
                    if(typeof inn == 'undefined'){
                        inn = '3e47b75000b0924b6c9ba5759a7cf15d';
                    }
                    if(typeof pba == 'undefined'){
                        pba = '3e47b75000b0924b6c9ba5759a7cf15d';
                    }
                    if(inn.indexOf(SearchText) > -1 || name.indexOf(SearchText) > -1 || pba.indexOf(SearchText) > -1){
                        $(val).removeClass('hidden');
                        searchcount++;
                    }else{
                        $(val).addClass('hidden');
                    }
                }).promise().done(function () {
                    $('.search_count').text(searchcount);
                });

                $('.service_group').each(function(index, val){ // проходим все группы и скрываем те, что не содержат сервисов
                    var service_containers = $(val).find('.service_container');
                    var hide = true;
                    $(service_containers).each(function(index, val){ // проходим все сервисы вроде быстро работает
                        //console.log($(val), $(val).hasClass('hidden'));
                        if(!$(val).hasClass('hidden')){
                            hide = false;
                        }
                    });
                    if(hide){
                        $(val).addClass('hidden');
                    }
                });

            }
        }

        function equalize(){

            $('[equalizer]').each(function () { // делает колонки одинаковой высоты
                var max_height = 0;
                $(this).find('[equal]').each(function () {
                    if($(this).height() > max_height){
                        max_height = $(this).height();
                    }
                });
                $(this).find('[equal]').each(function () {
                    $(this).height(max_height);
                });
            });

        }
        equalize();

        function renderServices(){

            for(var num in services){

                if(services[num].groupped == false){

                    $('#services_box').append(
                        "<a href='/services/"+id+"/form/"+services[num].service_id+"' equal class='service_container' data-service-inn='"+services[num].inn+"' data-service-name='"+services[num].www_name+"' data-service-pba='"+services[num].provider_bank_account+"'>" +
                        "<div class=\"service_header\">" +
                        "<span class=\"text\">"+services[num].name+"</span>" +
                        "</div>" +
                        "<div class=\"service_body\">" +
                        services[num].www_name +
                        "<div class=\"line\"></div>" +
                        "ИНН: <span class=\"pull-right\">"+services[num].inn+"</span>" +
                        "<div class=\"line\"></div>" +
                        "Р/С: <span class=\"pull-right\">"+services[num].provider_bank_account+"</span>" +
                        "</div>" +
                        "</a>"
                    );
                }else{

                    if(services[num].www_name == ''){
                        services[num].www_name = 'Другое';
                    }

                    $("#services_box").append(
                        "<a href='/services/"+id+"/groupby/"+services[num].service_id+"' equal class='service_container' data-service-name='"+services[num].www_name+"'>" +
                        "<div class=\"service_header\">" +
                        "<span class=\"text\">"+services[num].name+"</span>" +
                        "</div>" +
                        "<div class=\"service_body\">" +
                        services[num].www_name +
                        "<div class=\"line\"></div>" +
                        "С таким названием имеется несколько сервисов, нажмите чтобы увидеть подробности" +
                        "</div>" +
                        "</a>"
                    );
                }

            }
        }

        function addServices(data){
            for(var row in data){
                services.push(data[row]);
            }
        }

        return {
            loop: function (offset,step,service_id) {
                id = service_id;
                $.post('/lazyload', {'action':'lazyService','data': {'offset': offset, 'step': step, 'service_id': service_id}}, function (data) {
                    data = JSON.parse(data);
                    addServices(data.data);

                    renderServices();
                    equalize();
                    $('.loadhide').hide();
                    searchServices();
                    initToolTips();

                    //if(data.data.length){
                    //    searchServices();
                    //    lazyLoader.loop(Number(data.offset)+step, step, service_id);
                    //}else{
                    //    grouped_services = array_group(services, 'www_name');
                    //    renderServices();
                    //    equalize();
                    //    $('.loadhide').hide();
                    //    searchServices();
                    //    initToolTips();
                    //}
                });
            },
            getServices: function () {
                return services;
            }
        }
    })();
    lazyLoader.loop(0, 100, $('#services_box').attr('data-service-id'));

});