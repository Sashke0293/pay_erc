/**
 * Created by eremin_an on 10.03.2016.
 */

$(document).ready(function () {

    var tour_step = Lockr.get('tour_step');

    if(Lockr.get('tour_step') == 'undefined' || Lockr.get('tour_step') == null || Lockr.get('tour_step') == ''){
        Lockr.set('tour_step', 0);
    }

    function resetTour(){
        Lockr.set('tour_step', 0);
    }
    function incTour(){
        Lockr.set('tour_step', Lockr.get('tour_step') + 1);
    }
    function decTour(){
        Lockr.set('tour_step', Lockr.get('tour_step') - 1);
    }

    function initTour(){
        //$('#helper_disable').show();
        Lockr.set('tour_enabled', 1);
    }

    function startTour(){

        $(".links > li.active").tourTip({
            title: "Текущая страница",
            description: "Сейчас вы находитесь здесь",
            previous: true,
            position: 'bottom',
            onClose: function () {
                resetTour();
            },
            onPrevious: function(){
                decTour();
            }
        });

        $("#register").tourTip({
            title: "Регистрация",
            description: "Советую вам зарегистрироваться, так вы сможете создавать шаблоны и следить за платежами!",
            previous: true,
            position: 'right',
            onNext: function () {
                incTour();
            },
            onPrevious: function(){
                decTour();
            }
        });

        $("#socialIn").tourTip({
            title: "Регистрация",
            description: "Или войти с помощью своего аккаунта в социальной сети!",
            previous: true,
            position: 'right',
            onNext: function () {
                incTour();
            },
            onPrevious: function(){
                decTour();
            }
        });

        $("#popular").tourTip({
            title: "Наиболее частые платежи",
            description: "Мы собрали для вас наиболее часто совершаемые платежи!",
            previous: true,
            position: 'right',
            onNext: function () {
                incTour();
            },
            onPrevious: function(){
                decTour();
            }
        });

        $("#templatesBtn").tourTip({
            title: "Шаблоны",
            description: "Создавайте шаблоны чтобы оплачивать услуги быстрее!",
            previous: true,
            position: 'bottom',
            onNext: function () {
                incTour();
            },
            onPrevious: function(){
                decTour();
            }
        });

        $("#historyBtn").tourTip({
            title: "Платежи",
            description: "Здесь находится история всех совершенных вами платежей!",
            previous: true,
            position: 'bottom',
            onNext: function () {
                incTour();
            },
            onPrevious: function(){
                decTour();
            }
        });

        $("#tourStart").tourTip({
            title: "Услуги",
            description: "А теперь выберите услугу из меню",
            previous: true,
            position: 'right',
            close: true,
            onNext: function () {
                incTour();
            },
            onPrevious: function(){
                decTour();
            }
        });

    }

    $('.help_btn').click(function () {

        Lockr.set('tour_templates', 0);
        Lockr.set('tour_history', 0);
        Lockr.set('tour_service', 0);
        Lockr.set('tour_form', 0);
        Lockr.set('tour_pay', 0);
        resetTour();
        initTour();

        startTour();

        $.tourTip.start({
            nextButtonText: 'Далее',
            previousButtonText: 'Назад',
            closeButtonText: 'Закрыть'
        });
    });

});