/**
 * Created by eremin_an on 10.03.2016.
 */

$(document).ready(function () {

    $('#money_send').on('input', function(e){
        updateMoney();
    });

    function updateMoney(){

        //define vars
        var new_money_pay;
        var val = Number($('#money_send').val());
        var comission = Number($('#money_send').attr('data-comission'));
        var min = Number($('#money_send').attr('data-min'));
        var max = Number($('#money_send').attr('data-max'));
        var minfee = Number($('#money_send').attr('data-minfee'));
        var paymenttype = $('#money_send').attr('data-payment-type');

        //checking
        if(paymenttype == 'PAYMENT'){
            if( val * comission/100 < minfee){
                new_money_pay =  minfee + val;
            }else{
                new_money_pay = val * comission/100 + val;
            }
        }else if(paymenttype == 'CHECK'){
            if( (val/(100-comission)) * 100 - val < minfee){
                new_money_pay =  minfee + val;
            }else{
                new_money_pay = (val/(100-comission)) * 100;
            }
        }else{
            window.location = '/';
        }

        if(new_money_pay < min){
            $('[data-min-alert]').addClass('cRed');
        }else{
            $('[data-min-alert]').removeClass('cRed');
        }
        if(new_money_pay > max){
            $('[data-max-alert]').addClass('cRed');
        }else{
            $('[data-max-alert]').removeClass('cRed');
        }

        new_money_pay = Math.floor(Math.ceil(new_money_pay * 100))/100;
        $('#money_pay').val( new_money_pay );

    }
    updateMoney();

});