<?php
/**
 * Created by PhpStorm.
 * User: eremin_an
 * Date: 07.04.2016
 * Time: 16:09
 */

$paymentsSQL = "SELECT
                    pmts.params,
                    pmts.account,
                    pmts.enter_summ,
                    pmts.summ,
                    pmts.fee,
                    pmts.fee2,
                    ts.service_id,
                    ts.name AS service_name,
                    ts.contract_number,
                    ts.contract_start_date,
                    ts.provider_bank_account,
                    pr.org_form AS provider_org_form,
                    pr.name AS provider_name,
                    pr.inn AS provider_inn,
                    tp.name as parameter_name,
                    tsh.resulttext,
                    trs.create_date
                FROM
                    payments pmts
                        JOIN
                    t_service ts ON ts.service_id = pmts.service_id
                        JOIN
                    t_provider pr ON pr.provider_id = ts.provider_id
                        JOIN
                    t_parameter tp ON tp.service_id = ts.service_id and sort_order = 1
                        JOIN (SELECT rusr_id FROM reg_users where login = '$mail') as usr ON usr.rusr_id = pmts.rusr_rusr_id
						JOIN (SELECT resulttext FROM transaction_status_hist where trn_trn_id = $trn_id and resulttext like '%TransactionID:%') as tsh
						JOIN (SELECT create_date FROM transactions where trn_id = $trn_id) as trs
                WHERE
                    pmnt_id IN (SELECT
                            pmnt_pmnt_id
                        FROM
                            payment_transaction
                        WHERE
                            trn_trn_id = $trn_id);";

$payments = $this->app['db']->fetchAll($paymentsSQL);
$summ = 0;

if(count($payments) != 0){; ?>

<?php
foreach ($payments as $keypm => $payment) {
    $summ += $payment['summ'];
}
?>

<div style="padding:10px; border:1px; width:270px; font-family: Arial, Helvetica, sans-serif;  font-size:10px;">
    ООО "Единый Расчетный Центр"
    <br />
    ************************************************************
    <br />
    КВИТАНЦИЯ
    <br />
    Сайт www.erc-perm.ru
    <br />
    г. Пермь, ул. Гайдара, 8Б, офис 101, ИНН: 5906075678
    <br />

    <?php foreach($payments as $key  => $payment){ ?>
            ----------------------------------
            <br />
            Принято денежных средств: <?php echo money_format('%.2n', $payment['summ']); ?> руб.
            <br />
            В том числе комиссия: <?php echo money_format('%.2n', $payment['fee'] + $payment['fee2']); ?> руб.
            <br />
            Сумма без комиссии: <?php echo money_format('%.2n', $payment['summ'] - ($payment['fee'] + $payment['fee2'])); ?> руб.
            <br />
            <br />
            Получатель: <?php echo $payment['provider_org_form']; ?> <?php echo $payment['provider_name']; ?>,
            ИНН: <?php echo $payment['provider_inn']; ?>
            <br />
            Р/с: <?php echo $payment['provider_bank_account']; ?>
            <br />
            <?php echo $payment['service_name']; ?>
            <br />
            Номер договора: <?php echo $payment['contract_number']; ?>
            <br />
            Дата договора: <?php echo $payment['contract_start_date']; ?>
            <br />
            <br />
            <?php echo $payment['parameter_name']; ?>: <?php echo $payment['account']; ?>
            <br />
            <?php echo $payment['params']; ?>
            <br />
            ----------------------------------
    <?php }; ?>

    <br />
    <br />
    <?php echo $payments[0]['resulttext']; ?>
    <br />
    <br />
    Служба поддержки абонентов: (342) 270-01-37; 8-800-300-04-05;
    <br />
    ************************************************************
    <br />
    Спасибо за использование системы!
    <br />
    <br />
    <table  width="100%" cellspacing="0" cellpadding="0" style="font-family: Arial, Helvetica, sans-serif;  font-size:10px;">
        <tr>
            <td>Дата: <?php echo $payments[0]['create_date']; ?></td>
            <td align="right">Транзакция № <?php echo $trn_id; ?></td>
        </tr>
    </table>
    <table width="100%" cellspacing="0" cellpadding="0"  style="font-family: Arial, Helvetica, sans-serif;  font-size:10px;">
        <tr>
            <td>
                <h1>
                    ИТОГ:
                </h1>
            </td>
            <td align="right">
                <h1>
                    =<?php echo money_format('%.2n', $summ); ?>
                </h1>
            </td>
        </tr>
    </table>

    -----------------------------------------------------------------------------------<br />
    с банковской карты
</div>

<?php }else{

    $exists = false;

}; ?>