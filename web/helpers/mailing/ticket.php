
<table width="600" style="margin: auto;" cellpadding="0" cellspacing="0">
    <tr>
        <td style="padding: 10px;" colspan="2">
            <h1 style="text-align: center;">Регистрационные данные</h1>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <p style="text-align: center;">Вы успешно зарегистрировались на сайте erc-perm.ru!</p>
        </td>
    </tr>
    <tr>
        <td>
            Ваш логин:
        </td>
        <td>
            <?php echo $this->data['login']; ?>
        </td>
    </tr>
    <tr>
        <td>
            Ваш пароль:
        </td>
        <td>
            <?php echo $this->data['password']; ?>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <p>Вы можете посмотреть всю историю ваших действий на сайте выполнив вход в Личный кабинет.</p>
        </td>
    </tr>
</table>