<?php
/**
 * Created by PhpStorm.
 * User: eremin_an
 * Date: 16.03.2016
 * Time: 16:00
 */

$summa = 0;

foreach($app['session']->get('payments') as $payment){
    $summa += $payment['money_send']['value'];
}

$duration='+15 minutes';

$cur_date_time=date("Y-m-d H:i:s");

$valid_until=date('Y-m-d H:i:s', strtotime($duration, strtotime($cur_date_time)));
$cur_date_time_O=date('Y-m-d H:i:s', strtotime($duration, strtotime(gmdate("M d Y H:i:s", time()))));

$date_end_all='2020-12-12 00:00:00';

$payments_array = $app['session']->get('payments');
$app['session']->set('payments', null);

$orderDescr = 'Сервисы: ';

$Email = $app['session']->get('email');
if($Email == null || $Email == ''){
    $Email = $payments_array[0]['Email']['value'];
}

// заносим платежи
foreach($payments_array as $key1 => $pmnt){

    $converter = new Converter($app);
    $converter->fillConverterBy('session', $key1);
    $params = $converter->returnTemplate();
    $account = $converter->returnAccount();
    $params = $params['params'];

    $db_user = 'SELECT CURRENT_USER()';
    $sql = "SELECT CURRENT_USER() as cusr;";
    $db_user = $app['db']->fetchAssoc($sql);

    $db_user = $db_user['cusr'];

    $orderDescr .= $pmnt['service_id'].',';

    // в таблицу платежей
    $app['db']->insert("payments", array(
        "rusr_rusr_id" => $app['session']->get('login_id'),
        "service_id" => $pmnt['service_id'],
        "enter_summ" => $pmnt['money_send']['entered'],
        "summ" => $pmnt['money_send']['value'],
        "account" => $account,
        "create_date" => $cur_date_time,
        "params" => $params,
        "ext_fee_id" => $pmnt['ext_fee_id'],
        "ext2_fee_id" => $pmnt['ext2_fee_id'],
        "fee" => $pmnt['fee'],
        "fee2" => $pmnt['fee2'],
        "service_hash" => $pmnt['hash'],
        "user" => $db_user
    ));

    $Pid = $app['db']->lastInsertId();
    $payments_array[$key1]['pid'] = $Pid;
    // в историю платежей
    $app['db']->insert("payment_status_hist", array(
        "pmnt_pmnt_id" => $Pid,
        "pmntst_pmntst_id" => 4,
        "date_begin" => $cur_date_time,
        "date_end" => $date_end_all,
        "seq" => 1,
        "user" => $db_user
    ));
}
$orderDescr = substr($orderDescr, 0, -1);
$orderDescr = substr($orderDescr, 0, 100); // максимум 100 символов!

// добавляем транзакцию
$app['db']->insert("transactions", array(
    "create_date" => $cur_date_time,
    "valid_until" => $valid_until,
    "summ" => $summa,
    "plat_instr" => 1,
    "email" => $Email,
    "user" => $db_user
));
$OId = $app['db']->lastInsertId();
$app['session']->set('transaction_id', $OId);

// добавляем транзакцию в историю
$app['db']->insert("transaction_status_hist", array(
    "trn_trn_id" => $OId,
    "trnst_trnst_id" => 2,
    "date_begin" => $cur_date_time,
    "date_end" => $date_end_all,
    "user" => $db_user,
    "seq" => 1
));

// добавляю связь платежей и транзакций

// добавить проверку если такой платеж уже есть
foreach($payments_array as $key => $pmnt){
    $app['db']->insert("payment_transaction", array(
        "pmnt_pmnt_id" => $payments_array[$key]['pid'], // check this
        "trn_trn_id" => $OId,
        "date_begin" => $cur_date_time,
        "date_end" => $date_end_all,
        "user" => $db_user,
        "seq" => 1
    ));
    $pay_status = changePaymentStatus($app, $payments_array[$key]['pid'], '4', '', '');

    if($pay_status[1] < 0){
        $app['monolog']->addInfo(sprintf("Error: Проблемы с транзакцией %s у пользователя %s",
                $OId,
                $Email
            )
        );
        return $app->redirect('/errorPayment');
    }
}

$app['monolog']->addInfo(sprintf("Log: Пользователь '%s' выполнил транзакцию SELECT * FROM transactions WHERE trn_id = %s; SELECT * FROM payments WHERE pmnt_id IN (SELECT pmnt_pmnt_id FROM payment_transaction WHERE trn_trn_id = %s); mail = %s",
        $db_user,
        $OId,
        $OId,
        $Email
    )
);

//Указываем локализацию (доступно ru | en | fr)
$Language = "ru";
// Указываем идентификатор мерчанта
$MerchantId='14954';
//Указываем приватный ключ (см. в ЛК PayOnline в разделе Сайты -> настройка -> Параметры интеграции)
$PrivateSecurityKey='d26cd63a-72c3-4d32-b000-101f94898a4e';
//Номер заказа (Строка, макс.50 символов)
$OrderId=$OId;
//Валюта (доступны следующие валюты | USD, EUR, RUB)
$Currency='RUB';
//Сумма к оплате (формат: 2 знака после запятой, разделитель ".")
$Amount=$summa;
//Описание заказа (не более 100 символов, запрещено использовать: адреса сайтов, email-ов и др.) необязательный параметр
$OrderDescription=$orderDescr;
//Срок действия платежа (По UTC+0) необязательный параметр
//$ValidUntil="2013-10-10 12:45:00";
//В случае неуспешной оплаты, плательщик будет переадресован, на данную страницу.
$FailUrl="erc-perm/errorPayment";
// В случае успешной оплаты, плательщик будет переадресован, на данную страницу.
$ReturnUrl="erc-perm/successPayment";

//Создаем класс
$pay = new GetPayment();

$pay->Language=$Language;
$pay->MerchantId=$MerchantId;
$pay->PrivateSecurityKey=$PrivateSecurityKey;
$pay->OrderId=$OrderId;
$pay->Amount=number_format($Amount, 2, '.', '');
$pay->Currency=$Currency;
$pay->OrderDescription=$OrderDescription;
$pay->ValidUntil=$cur_date_time_O;
$pay->ReturnUrl=$ReturnUrl;
$pay->Email=$Email;
$pay->FailUrl=$FailUrl;

//Показываем ссылку на оплату

function changePaymentStatus($app, $payid, $statusid, $rezultcode, $rezulttext){

    $app['db']->executeQuery("SET @r1 = 0, @r2 = ''");
    $app['db']->executeQuery("CALL `change_payment_status`('$payid', '$statusid', '$rezultcode', '$rezulttext', true, @r1, @r2);");
    $row = $app['db']->fetchArray("SELECT @r1 as r1, @r2 as r2");

    $pay_status = array(
        1 => $row[0],
        2 => $row[1]
    );

    return $pay_status;
}

?>