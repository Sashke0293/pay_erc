<?php

require_once __DIR__.'/../vendor/autoload.php';

$app = new Silex\Application();

use Symfony\Component\Templating\PhpEngine;
use Symfony\Component\Templating\TemplateNameParser;
use Symfony\Component\Templating\Loader\FilesystemLoader;
use Symfony\Component\Templating\DelegatingEngine;

use Symfony\Component\HttpFoundation\Response;

use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\Request;
use Silex\Application;

include "classes/Converter.php";
include "classes/Authoriser.php";
include "classes/User.php";
include "classes/ajax.php";
include "classes/Mailing.php";

//$app['swiftmailer.options'] = array(
//    'host' => 'email.erc-perm.ru',
//    'port' => 587,
//    'username' => 'noreply',
//    'password' => 'Gijnds78^',
//    'encryption' => 'ssl',
//    'auth_mode' => null
//);
//$app['swiftmailer.use_spool'] = false;

// регистрирую подключение к базе
$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'dbs.options' => array(
        'pay' => array(
            'driver' => 'pdo_mysql',
            'dbname' => 'erc-site',
            'host' => 'localhost',
            'user' => 'root',
            'password' => 'Cfqnpltcm!23',
            'charset' => 'utf8'
        ),
        'wp' => array(
            'driver' => 'pdo_mysql',
            'dbname' => 'wp-erc-site',
            'host' => 'localhost',
            'user' => 'root',
            'password' => 'Cfqnpltcm!23',
            'charset' => 'utf8'
        )
    )
));

// мыло
$app->register(new Silex\Provider\SwiftmailerServiceProvider());

// Перевод
$app->register(new Silex\Provider\TranslationServiceProvider(), array(
    'locale_fallbacks' => array('ru'),
));

// кэширование
//$app->register(new Silex\Provider\HttpCacheServiceProvider(), array(
//    'http_cache.cache_dir' => __DIR__.'/cache/',
//));

// подключаю Twig шаблонизатор
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/views',
));

// подключаю Сессии
$app->register(new Silex\Provider\SessionServiceProvider());

// подключаю валидатор
$app->register(new Silex\Provider\ValidatorServiceProvider());

$app->register(new Silex\Provider\MonologServiceProvider(), array(
    'monolog.logfile' => __DIR__.'/development.log',
));

$app['templating.engines'] = $app->share(function() {
    return array(
        'twig',
        'php'
    );
});

$user = null;
if($app['session']->get('email') != null && $app['session']->get('password') != null){
    $user = User::getInstance(array(
        'app' => $app,
        'data' => array(
            'source' => 'session',
            'login' => $app['session']->get('email'),
            'password' => $app['session']->get('password')
        )
    ));
    if(!User::checkUserObject($user)){
        $app['session']->remove('auth_type');
        $app['session']->remove('login_id');
        $app['session']->remove('password');
        $app['session']->remove('email');
        $app['session']->remove('user_name');
        $app['session']->remove('user_status');
        $app['session']->remove("user_email");
    }
}

$app->before(function () use ($app,$user) {

    if(User::checkUserObject($user)){
        $menu = array(
            array(
                'path' => '/',
                'name' => 'Каталог получателей',
                'active' => false
            ),
            array(
                'path' => '/#',
                'name' => 'О сервисе',
                'active' => false
            ),
            array(
                'path' => '/templates',
                'name' => 'Шаблоны',
                'id' => 'templatesBtn',
                'active' => false
            ),
            array(
                'path' => '/history',
                'name' => 'Платежи',
                'id' => 'historyBtn',
                'active' => false
            )
        );
    }else{

        $menu = array(
            array(
                'path' => '/',
                'name' => 'Каталог получателей',
                'active' => false
            ),
            array(
                'path' => '/#',
                'name' => 'О сервисе',
                'active' => false
            )
        );
    }

    if(User::checkUserObject($user)){
        $app['twig']->addGlobal('user', $user->getUser());
    }

//    var_dump($user->getUser());

    foreach($menu as $key => $row){
        if($app['request']->getRequestUri() == $row['path']){
            $menu[$key]['active'] = true;
        }
    }

    $app['twig']->addGlobal('menu', $menu);

    $app['twig']->addGlobal('layout', $app['twig']->loadTemplate('layout.twig'));
    $payments = $app['session']->get('payments');
    $app["twig"]->addGlobal("payments", $payments);

    $sql = "SELECT * FROM t_service JOIN (SELECT service_id FROM payments GROUP BY service_id order by count(service_id) DESC LIMIT 40) as popt ON popt.service_id = t_service.service_id where active <> 0 LIMIT 5;";
    $popular = $app['dbs']['pay']->fetchAll($sql);
    $app["twig"]->addGlobal("popular", $popular);

    $app["twig"]->addGlobal("erc_status", array(
        "Считаю комиссию...",
        "Отправляю платежи...",
        "Проверяю почту...",
        "Завариваю чай..."
    ));

    if(User::checkUserObject($user)){

        $sql = "SELECT * FROM payments WHERE rusr_rusr_id = ".$user->id.";";
        $lastpays = $app['dbs']['pay']->fetchAll($sql);
        $app["twig"]->addGlobal("lastpays", $lastpays);
    }
});

// =============================================================
// ================== ENABLE PHP TEMPLATING ====================
// =============================================================

$app['templating.loader'] = $app->share(function() {
    return new FilesystemLoader(__DIR__.'/views/%name%');
});

$app['templating.template_name_parser'] = $app->share(function() {
    return new TemplateNameParser();
});

$app['templating.engine.php'] = $app->share(function() use ($app) {
    return new PhpEngine($app['templating.template_name_parser'], $app['templating.loader']);
});

$app['templating.engine.twig'] = $app->share(function() use ($app) {
    return new TwigEngine($app['twig'], $app['templating.template_name_parser']);
});

$app['debug'] = true;

$app['templating'] = $app->share(function() use ($app) {
    $engines = array();

    foreach ($app['templating.engines'] as $i => $engine) {
        if (is_string($engine)) {
            $engines[$i] = $app[sprintf('templating.engine.%s', $engine)];
        }
    }

    return new DelegatingEngine($engines);
});

// =============================================================
// ========================== ROUTING ==========================
// =============================================================

$app->get('/', function () use ($app) {
    $sql = "SELECT * FROM pays_groups WHERE name IN (SELECT DISTINCT pg.name FROM pays_groups pg JOIN t_service ts ON ts.service_type_id = pg.id);";
    $post = $app['dbs']['pay']->fetchAll($sql);
//    $app['session']->set('payments', null);

    return $app['twig']->render('blocks/main.twig', array(
        'pays_groups' => $post
    ));
});

$app->get('/templates', function () use ($app,$user) {
    if(User::checkUserObject($user)){

        $payment_templates = "CREATE TABLE IF NOT EXISTS `payment_templates` (
                  `tmp_id` int(11) NOT NULL AUTO_INCREMENT,
                  `service_id` int(11) NOT NULL,
                  `params` text,
                  `rusr_rusr_id` int(11) NOT NULL,
                  `summ` float DEFAULT NULL,
                  PRIMARY KEY (`tmp_id`)
                ) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;";

        $post = $app['dbs']['pay']->executeQuery($payment_templates);

        $sql = "SELECT pt.*, ts.www_name, ts.service_type_id FROM payment_templates pt JOIN t_service ts ON pt.service_id = ts.service_id WHERE rusr_rusr_id = ".$app['session']->get('login_id');
        $post = $app['dbs']['pay']->fetchAll($sql);

        foreach($post as $key => $template){
            $post[$key]['params'] = explode(';',$post[$key]['params']);
            $post[$key]['params'] = array_filter ($post[$key]['params']);
            foreach($post[$key]['params'] as $key1 => $param){
                $post[$key]['params'][$key1] = explode(': ',$post[$key]['params'][$key1]);
                $post[$key]['params'][$key1] = array_filter ($post[$key]['params'][$key1]);
            }
        }

        return $app['twig']->render('blocks/templates.twig', array(
            'post' => $post
        ));
    }else{
        return $app->redirect('/');
    }
});

//$app->get('/cache', function () use ($app) {
//    return new Response('Foo', 200, array(
//        'Cache-Control' => 's-maxage=5',
//    ));
//});


$app->get('/services', function () use ($app) {
    return $app->redirect('/');
});

$app->get('/indications', function () use ($app) {
    return $app['twig']->render('blocks/indications.twig');
});

$app->post('/lazyload', function (Request $request) use ($app) {

    $action = $request->get('action');
    $data = $request->get('data');
    $ajax = new Ajax($app);

    if($action != null){
        if($data != null){
            $response = $ajax->$action($data);
        }else{
            $response = $ajax->$action();
        }
    }

    return json_encode(array('status' => 'success', 'offset' => $data['offset'], 'data' => $response));
});

$app->get('/services/{id}', function ($id) use ($app) {
    $sql = "SELECT name FROM pays_groups WHERE id = ?;";
    $service = $app['dbs']['pay']->fetchAssoc($sql, array((int) $id));

    $ajax = new Ajax($app);


    $app['monolog']->addInfo(sprintf("Начало1: %s", time()));
    $services = $ajax->getServices(array(
        'service_id' => $id
    ));
    $app['monolog']->addInfo(sprintf("Конец1: %s", time()));

//    $sql = "SELECT ts.*, tp.inn FROM t_service ts JOIN t_provider tp ON tp.provider_id = ts.provider_id WHERE service_type_id = ? and ts.active = 1;";
//    $post = $app['dbs']['pay']->fetchAll($sql, array((int) $id));

    return $app['twig']->render('blocks/service.twig', array(
        't_service' => $services,
        'service_id' => $id,
        'path_back' => array(
            0 => array(
                'name' => 'Выбор сервиса',
                'path' => '/',
                'class' => 'breadcrump'
            ),
            1 => array(
                'name' => $service['name'],
                'path' => '/services/'.$id,
                'class' => 'breadcrump active'
            ),
        )
    ));
});

$app->get('/services/{sid}/form/{id}', function ($sid, $id) use ($app) {

    $converter = new Converter($app);

    $form = $converter->getEmptyForm($id);

    if($form['service']['service_type_id'] == $sid){
        return $app['twig']->render('blocks/form.twig', array(
            'form' => $form,
            'hash' => md5($form['hash']),
            'path_back' => array(
                0 => array(
                    'name' => 'Выбор сервиса',
                    'path' => '/',
                    'class' => 'breadcrump'
                ),
                1 => array(
                    'name' => $form['service']['service_type'],
                    'path' => '/services/'.$sid,
                    'class' => 'breadcrump'
                ),
                2 => array(
                    'name' => $form['service']['www_name'],
                    'path' => '/services/'.$sid.'/form/'.$id,
                    'class' => 'breadcrump active'
                ),
            )
        ));
    }else{
        return $app->redirect('/');
    }

});

$app->get('/services/{sid}/groupby/{id}', function ($sid, $id) use ($app) {

    $sql = "SELECT name FROM pays_groups WHERE id = ?;";
    $service = $app['dbs']['pay']->fetchAssoc($sql, array((int) $sid));

    $sql = "SELECT ts.*, tp.inn FROM t_service ts JOIN t_provider tp ON tp.provider_id = ts.provider_id WHERE ts.www_name in (SELECT www_name from t_service where service_id = ?) and ts.active = 1;";
    $services = $app['dbs']['pay']->fetchAll($sql, array((int) $id));


    return $app['twig']->render('blocks/serviceNotAjax.twig', array(
        't_service' => $services,
        'service_id' => $id,
        'path_back' => array(
            0 => array(
                'name' => 'Выбор сервиса',
                'path' => '/',
                'class' => 'breadcrump'
            ),
            1 => array(
                'name' => $service['name'],
                'path' => '/services/'.$sid,
                'class' => 'breadcrump active'
            ),
            2 => array(
                'name' => 'Группа('.$services[0]['www_name'].')',
                'path' => '/services/'.$sid,
                'class' => 'breadcrump active'
            )
        )
    ));

});


$app->post('/services/{sid}/form/{id}', function ($sid, $id, Request $request) use ($app) {

    try{
        $converter = new Converter($app);

        $tmp_id = $request->get('tmp_id');
        $pmnt_id = $request->get('pmnt_id');
        $ses_id = $request->get('ses_id');

        $payments = $app['session']->get('payments');

        if($tmp_id != null){
            $converter->fillConverterBy('template',$tmp_id);
            unset($payments[$tmp_id]);
        }else if($pmnt_id != null){
            $converter->fillConverterBy('payment',$pmnt_id);
            unset($payments[$pmnt_id]);
        }else{
            $converter->fillConverterBy('session',$ses_id);
            unset($payments[$ses_id]);
        }

        $app["twig"]->addGlobal("payments", $payments);

        $form = $converter->returnForm();


        if($tmp_id != null){
            $form['type'] = 'template';
            $form['id'] = $tmp_id;
        }else if($pmnt_id != null){
            $form['type'] = 'payment';
            $form['id'] = $pmnt_id;
        }else{
            $form['type'] = 'session';
            $form['id'] = $ses_id;
        }

        return $app['twig']->render('blocks/form_template.twig', array(
            'form' => $form,
            'email' => $app['session']->get('email'),
            'hash' => md5($form['hash']), // генерируется не такой как на сайте
            'path_back' => array(
                0 => array(
                    'name' => 'Выбор сервиса',
                    'path' => '/',
                    'class' => 'breadcrump'
                ),
                1 => array(
                    'name' => $form['service']['service_type'],
                    'path' => '/services/'.$sid,
                    'class' => 'breadcrump'
                ),
                2 => array(
                    'name' => $form['service']['www_name'],
                    'path' => '/services/'.$sid.'/form/'.$id,
                    'class' => 'breadcrump active'
                ),
            )
        ));

    }catch (Exception $e){
        $app['monolog']->addInfo(sprintf("ОШИБКА: %s", $e));
        return $app->redirect('/');
    }

});

$app->get('/history', function () use ($app, $user) {

    if(User::checkUserObject($user)){
        $sql = "SELECT pmt.*, ts.provider_bank_name as pbn, tstat.short_name as transact_name FROM transactions trn JOIN payment_transaction pt ON pt.trn_trn_id = trn.trn_id JOIN payments pmt ON pmt.pmnt_id = pt.pmnt_pmnt_id JOIN t_service ts ON ts.service_id = pmt.service_id JOIN transaction_status_hist tsh ON tsh.trn_trn_id = trn.trn_id JOIN transaction_status tstat ON tstat.trnst_id = tsh.trnst_trnst_id WHERE trn.user = ? ORDER BY pmt.create_date DESC;";
        $transactions = $app['dbs']['pay']->fetchAll($sql, array((int) $app['session']->get('login_id')));

        foreach($transactions as $key => $template){
            $transactions[$key]['params'] = explode(';',$transactions[$key]['params']);
            $transactions[$key]['params'] = array_filter ($transactions[$key]['params']);
            foreach($transactions[$key]['params'] as $key1 => $param){
                $transactions[$key]['params'][$key1] = explode(':',$transactions[$key]['params'][$key1]);
                $transactions[$key]['params'][$key1] = array_filter ($transactions[$key]['params'][$key1]);
            }
        }

        return $app['twig']->render('blocks/history.twig', array(
            'transactions' => $transactions
        ));
    }else{
        return $app->redirect('/');
    }
});

$app->get('/superuser', function () use ($app, $user) {

    if(User::checkUserObject($user)){

        $newUser = $user->getUser();

        if($newUser['login'] == 'sashkeer@gmail.com'){

            return $app['twig']->render('blocks/admin.twig');
        }else{
            return $app->redirect('/');
        }

    }else{
        return $app->redirect('/');
    }
});

$app->post('/payonline', function () use ($app,$user) {
    include "classes/payment.class.php";
    if(!User::checkUserObject($user)){
        $app['session']->set('login_id', 1);
    }
    $pay = null;
    include "helpers/payonline.php";
    $result=$pay->GetPaymentURL();
    return $app->redirect($result);
});

$app->get('/json', function () use ($app, $user) {

    $converter = new Converter($app);

    session_start();

    var_dump($_SESSION);

//    var_dump($app['session']->get('payments'));

//    $converter->fillConverterBy('session', 0);
//
//    var_dump($converter->returnSession());

    return json_encode($app['session']->get('payments'));
});

$app->get('/register', function () use ($app,$user) {

    if(!User::checkUserObject($user)){
        return $app['twig']->render('blocks/register.twig');
    }else{
        return $app->redirect('/');
    }
});

$app->get('/errorPayment', function () use ($app,$user) {

    if(User::checkUserObject($user)){
        $app['session']->set('payments', null);

        $message = \Swift_Message::newInstance()
            ->setSubject('SiteError')
            ->setFrom(array('pay.erc-perm@mail.ru'))
            ->setTo(array('alex0293@mail.ru'))
            ->setBody(
                "На сайте возникла ошибка у пользователя ".$app['session']->get('email')
            );

        $app['mailer']->send($message);

        return $app['twig']->render('blocks/payerror.twig', array());
    }else{
        return $app->redirect('/');
    }
});

$app->match('/successPayment', function () use ($app,$user) {

//    $mpdf=new mPDF();
//    $mpdf->WriteHTML("<h1 style='text-align: center;'>test</h1>");
//    $mpdf->Output('../pdfiles/'.md5().'.pdf', 'F');
    if(User::checkUserObject($user)){
        $app['session']->set('payments', null);
        return $app['twig']->render('blocks/paysuccess.twig', array());
    }else{
        return $app->redirect('/');
    }
});

$app->match('/forgot', function () use ($app,$user) {

    return $app->redirect('/');
});

$app->match('/reset', function () use ($app) {

    $app['session']->remove('auth_type');
    $app['session']->remove('login_id');
    $app['session']->remove('password');
    $app['session']->remove('email');
    $app['session']->remove('user_name');
    $app['session']->remove('user_status');
    $app['session']->remove("user_email");
    $app['session']->set('payments', null);
    return $app->redirect('/');
});

$app->post('/ajax', function (Request $request) use ($app) {
    $action = $request->get('action');
    $data = $request->get('data');
    $ajax = new Ajax($app);

    $response = null;

    if($action != null){
        if($data != null){
            return $ajax->$action($data);
        }else{
            return $ajax->$action();
        }
    }

});

$app->get('/feedback', function () use ($app) {

    $mailer = new Mailing();

//    $headers   = array();
//    $headers[] = "MIME-Version: 1.0";
//    $headers[] = "Content-type: text/plain; charset=utf8";
//    $headers[] = "From: ЕРЦ <erc-perm@erc-perm.ru>";
//    $headers[] = "Subject: Feedback";
//    $headers[] = "X-Mailer: PHP/".phpversion();

    $email = $app['session']->get('email');

    if($email != null){
        $mailer->to = $email;
        $mailer->subject = "Регистрация";
        $mailer->type = "ticket";

        $mailer->sendMail();
        return new Response('Thank you for your feedback!', 201);
    }else{
        return new Response('Вы кто?',404);
    }
});
$app->get('/restore/{hash}', function ($hash) use ($app) {
    $sql = "SELECT * FROM user_restore WHERE hash = ? and sysdate + interval 2 day > now() order by sysdate;";

    $row = $app['dbs']['pay']->fetchAssoc($sql, array($hash));

    if($row == false){
        return $app->redirect('/');
    }else{
        return $app['twig']->render('blocks/restorepassword.twig', array(
            'hash' => $hash
        ));
    }

});
//$app->error(function (\Exception $e, $code) use ($app){
//    switch ($code) {
//        case 404:
//            return $app['twig']->render('blocks/404.twig', array());
//            break;
//        default:
//            $message = 'Прошу прощения, но я не могу отобразить текущую страницу! Вы будете перенаправлены на главную. Ошибка: '.$code;
//            return new Response($message);
//    }
//
//});

//TODO сделать модель

$app->run();
